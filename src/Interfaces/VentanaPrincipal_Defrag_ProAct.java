package Interfaces;

import EON.Utilitarios.*;
import EON.*;
import EON.Algoritmos.*;
import EON.Metricas.Metricas;
import static EON.Utilitarios.Utilitarios.Dijkstra;
import static EON.Utilitarios.Utilitarios.quickSortDemandasDescendente;
import static EON.Utilitarios.Utilitarios.quicksortDemandas;
import static EON.Utilitarios.Utilitarios.quicksortDijkstra;
import com.formdev.flatlaf.FlatDarkLaf;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import org.jfree.data.xy.*;
import org.jfree.chart.annotations.XYTextAnnotation;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author sGaleano - dBaez Frame que se encargad de la interfaz con el usurio y
 * realizar la simulacion de una Red Optica Elastica. Permite realizar una
 * simulacion teniendo: - Una topologia - Un conjunto de algoritmos. - Un tipo
 * de demanda que sera generada y guardada en un archivo.
 */
public class VentanaPrincipal_Defrag_ProAct extends javax.swing.JFrame {

    private final Topologias Redes; // topologias disponibles

    private int tiempoTotal; // Iiempo que dura una simualcion
    private String redSeleccionada;
    private double[][][] topologia;
    private double anchoFS, probBloqueoBth, ventanaPar; // ancho de un FS en los enlaces
    private int capacidadPorEnlace; // cantidad de FSs por enlace en la topologia elegida

    private String metodo;
    private String tipoTrafico;
    private String metrica;

    private int Erlang;
    private boolean esBloqueo;
    private boolean encontroSolucion = false;
    private boolean encontroSolucionAG = false;
    private int Lambda, contBloqueos;
    private int HoldingTime; // Erlang / Lambda
    private int FsMinimo; // Cantidad mínima de FS por enlace
    private int FsMaximo; // Cantidad máxima de FS por enlace
    private double abpgrafo, shanonEntropy, bjj678, entropia, msi, bfr, pathConsec, entropiaUso, porcUso, probBloqueo;
    private ArrayList<ArrayList<Integer>> rutasEstablecidas; //guarda el tiempo de vida de las rutas ya establecidas por el algoritmo RSA
    private ArrayList<ArrayList<ListaEnlazada>> arrayRutas;//Guarda la lista enlazada que representa a la ruta establecida por el algoritmo RSA
    private ArrayList<ArrayList<Resultado>> resultadoRuteo;//Guarda los resutados del algoritmo para saber en que FS fue ubicada la demanda
    private ArrayList<ArrayList<ListaEnlazada[]>> listaKSP;
    int hora, minutos, segundos, dia, mes, anho;

    private final List algoritmosCompletosParaGraficar;
    private int cantidadDeAlgoritmosTotalSeleccionados;
    private boolean desfragmentarFS;
    private String ordenarDemandas;

    private int cores;

    public VentanaPrincipal_Defrag_ProAct() {
        initComponents();
        this.Redes = new Topologias(); // asignamos todas las topologias disponibles}

        /*No mostramos inicialmente los paneles que muestran los Resultados
         */
        //this.cantidadDeAlgoritmosRuteoSeleccionados = 0;
        this.cantidadDeAlgoritmosTotalSeleccionados = 0;
        //this.algoritmosCompletosParaEjecutar = new LinkedList();
        this.algoritmosCompletosParaGraficar = new LinkedList();
        this.setTitle("EON Simulator - Defragmentación ProActiva");

        setearRed(); // setea la red que aparece por defecto

        // Al inicio de cada Simulacion e+condemos los paneles de Resultado
        this.etiquetaTextoBloqueosTotales.setVisible(false);
        this.etiquetaDemandasTotales.setVisible(false);
        this.etiquetaTextoDemandasTotales.setVisible(false);
        this.etiquetaBloqueosTotales.setVisible(false);
        this.etiquetaCantDesfrag.setVisible(false);
        this.etiquetaCantDesfrag.setVisible(false);
        this.etiquetaCantRutasReruteadas.setVisible(false);
        this.etiquetaCantDesfrag.setVisible(false);
        this.etiquetaTextoCantRutasReruteadas.setVisible(false);
        this.etiquetaTextoCantDesfrag.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        listaAlgoritmosRuteo = new javax.swing.JList<>();
        botonEjecutarSimulacion = new javax.swing.JButton();
        etiquetaTopologia = new javax.swing.JLabel();
        etiquetaCapacidadActual = new javax.swing.JLabel();
        etiquetaTiempoActual = new javax.swing.JLabel();
        spinnerTiempoSimulacion = new javax.swing.JSpinner();
        jLabel2 = new javax.swing.JLabel();
        etiquetaImagenTopologia = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        etiquetaDemandasTotales = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        spinnerErlang = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        textFieldCapacidadEnlace = new javax.swing.JTextField();
        listaRedes = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        etiquetaAnchoFSActual1 = new javax.swing.JLabel();
        textFieldLambda = new javax.swing.JTextField();
        etiquetaAnchoFSActual2 = new javax.swing.JLabel();
        textFieldFSminimo = new javax.swing.JTextField();
        etiquetaAnchoFSActual3 = new javax.swing.JLabel();
        etiquetaAnchoFSActual4 = new javax.swing.JLabel();
        textFieldFSmaximo = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        textFieldAnchoFS = new javax.swing.JTextField();
        etiquetaAnchoFSActual = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        etiquetaTextoBloqueosTotales = new javax.swing.JLabel();
        etiquetaBloqueosTotales = new javax.swing.JLabel();
        etiquetaTextoDemandasTotales = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        etiquetaTextoMax = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableResultadosBloqueosMinMax = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableResultadosBloqueos = new javax.swing.JTable();
        etiquetaTextoMin = new javax.swing.JLabel();
        etiquetaRSA1 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTableResultados = new javax.swing.JTable();
        etiquetaRSA3 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTableResultadosMinMax = new javax.swing.JTable();
        etiquetaTextoMin1 = new javax.swing.JLabel();
        etiquetaTextoMax1 = new javax.swing.JLabel();
        panelResultados = new javax.swing.JScrollPane();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        jScrollPane6 = new javax.swing.JScrollPane();
        jTableEstadoEnlaces = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTableResultadosDefrag = new javax.swing.JTable();
        etiquetaRSA2 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        etiquetaAnchoFSActual19 = new javax.swing.JLabel();
        textFieldPeriodoDesfrag = new javax.swing.JTextField();
        etiquetaAnchoFSActual22 = new javax.swing.JLabel();
        etiquetaTextoCantRutasReruteadas = new javax.swing.JLabel();
        etiquetaCantRutasReruteadas = new javax.swing.JLabel();
        etiquetaCantDesfrag = new javax.swing.JLabel();
        etiquetaTextoCantDesfrag = new javax.swing.JLabel();
        ComboMetodoDesfrag = new javax.swing.JComboBox<>();
        ComboMetodo = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        etiquetaAnchoFSActual26 = new javax.swing.JLabel();
        textFieldRutasARerutear = new javax.swing.JTextField();
        etiquetaAnchoFSActual12 = new javax.swing.JLabel();
        etiquetaTopologia2 = new javax.swing.JLabel();
        ComboObjetivoReruteo = new javax.swing.JComboBox<>();
        jPanel5 = new javax.swing.JPanel();
        etiquetaTopologia1 = new javax.swing.JLabel();
        ComboObjetivoACO = new javax.swing.JComboBox<>();
        etiquetaAnchoFSActual8 = new javax.swing.JLabel();
        textFieldCantHormigas = new javax.swing.JTextField();
        etiquetaAnchoFSActual20 = new javax.swing.JLabel();
        textFieldMejoraACO = new javax.swing.JTextField();
        etiquetaAnchoFSActual6 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        ComboObjAlgoritmoGenetico = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jTextFieldCantIndividuosAG = new javax.swing.JTextField();
        label1 = new java.awt.Label();
        jTextFieldCantGeneraciones = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldMejoraAG = new javax.swing.JTextField();
        etiquetaAnchoFSActual7 = new javax.swing.JLabel();
        jTextFieldProbBloqueo = new javax.swing.JTextField();
        etiquetaProbBloqueo = new javax.swing.JLabel();
        jTextFieldVentana = new javax.swing.JTextField();
        etiquetaVentana = new javax.swing.JLabel();
        ComboMetrica = new javax.swing.JComboBox();
        jLabel15 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        ComboTipoTrafico = new javax.swing.JComboBox<>();
        jLabel16 = new javax.swing.JLabel();
        textFieldCores = new javax.swing.JTextField();
        checkDesfragFS = new javax.swing.JCheckBox();
        etiquetaAnchoFSActual23 = new javax.swing.JLabel();
        ComboOrdenarDemandas = new javax.swing.JComboBox<>();
        etiquetaError = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setLocationByPlatform(true);
        setMinimumSize(new java.awt.Dimension(1366, 800));
        setPreferredSize(new java.awt.Dimension(1600, 900));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        listaAlgoritmosRuteo.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "FA", "FA-CA", "MTLSC" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        listaAlgoritmosRuteo.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        listaAlgoritmosRuteo.setToolTipText("");
        listaAlgoritmosRuteo.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        listaAlgoritmosRuteo.setSelectedIndex(0);
        listaAlgoritmosRuteo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listaAlgoritmosRuteoMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(listaAlgoritmosRuteo);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 360, 90, 60));

        botonEjecutarSimulacion.setText("Play");
        botonEjecutarSimulacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEjecutarSimulacionActionPerformed(evt);
            }
        });
        getContentPane().add(botonEjecutarSimulacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 690, 100, 20));

        etiquetaTopologia.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        etiquetaTopologia.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        etiquetaTopologia.setText("Topologia");
        getContentPane().add(etiquetaTopologia, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 90, 60, 20));

        etiquetaCapacidadActual.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiquetaCapacidadActual.setText("Capacidad");
        getContentPane().add(etiquetaCapacidadActual, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 60, 20));

        etiquetaTiempoActual.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiquetaTiempoActual.setText("Tiempo de Simulacion");
        getContentPane().add(etiquetaTiempoActual, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 130, 20));

        spinnerTiempoSimulacion.setModel(new javax.swing.SpinnerNumberModel(50, 50, 100000, 25));
        spinnerTiempoSimulacion.setToolTipText("");
        spinnerTiempoSimulacion.setRequestFocusEnabled(false);
        spinnerTiempoSimulacion.setValue(1000);
        getContentPane().add(spinnerTiempoSimulacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 90, 60, 20));

        jLabel2.setText("FSs por Enlace");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 120, -1, 20));

        etiquetaImagenTopologia.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiquetaImagenTopologia.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        etiquetaImagenTopologia.setFocusable(false);
        etiquetaImagenTopologia.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        etiquetaImagenTopologia.setOpaque(true);
        etiquetaImagenTopologia.setVerifyInputWhenFocusTarget(false);
        getContentPane().add(etiquetaImagenTopologia, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 380, 150));

        jLabel5.setText("unid.");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 90, -1, 20));

        etiquetaDemandasTotales.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        getContentPane().add(etiquetaDemandasTotales, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 20, 50, 20));

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Tráfico Máximo");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 340, 90, 20));

        spinnerErlang.setModel(new javax.swing.SpinnerNumberModel(400, 1, 1500, 50));
        getContentPane().add(spinnerErlang, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 340, 60, -1));

        jLabel6.setText("Erlang");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 340, 50, 20));

        textFieldCapacidadEnlace.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        textFieldCapacidadEnlace.setText("320");
        textFieldCapacidadEnlace.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldCapacidadEnlaceActionPerformed(evt);
            }
        });
        getContentPane().add(textFieldCapacidadEnlace, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 120, 50, -1));

        listaRedes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "NSFNet", "USNet", "ARPA-2" }));
        listaRedes.setSelectedIndex(1);
        listaRedes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listaRedesActionPerformed(evt);
            }
        });
        getContentPane().add(listaRedes, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 90, 80, -1));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel10.setText("Desfragmentación ProActiva");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 10, -1, -1));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel11.setText("Resultados");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 10, -1, -1));

        etiquetaAnchoFSActual1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiquetaAnchoFSActual1.setText("Lambda");
        getContentPane().add(etiquetaAnchoFSActual1, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 340, 50, 20));

        textFieldLambda.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        textFieldLambda.setText("5");
        textFieldLambda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldLambdaActionPerformed(evt);
            }
        });
        getContentPane().add(textFieldLambda, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 340, 30, 30));

        etiquetaAnchoFSActual2.setText("mín");
        getContentPane().add(etiquetaAnchoFSActual2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 400, 30, 20));

        textFieldFSminimo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        textFieldFSminimo.setText("1");
        textFieldFSminimo.setToolTipText("");
        textFieldFSminimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldFSminimoActionPerformed(evt);
            }
        });
        getContentPane().add(textFieldFSminimo, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 400, 30, 20));

        etiquetaAnchoFSActual3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiquetaAnchoFSActual3.setText("FS Rango");
        getContentPane().add(etiquetaAnchoFSActual3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 400, 60, 20));

        etiquetaAnchoFSActual4.setText("máx");
        getContentPane().add(etiquetaAnchoFSActual4, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 400, 30, 20));

        textFieldFSmaximo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        textFieldFSmaximo.setText("8");
        textFieldFSmaximo.setToolTipText("");
        textFieldFSmaximo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldFSmaximoActionPerformed(evt);
            }
        });
        getContentPane().add(textFieldFSmaximo, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 400, 30, 20));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel12.setText("Red");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, -1, -1));

        jLabel3.setText("GHz");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 120, 30, 20));

        textFieldAnchoFS.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        textFieldAnchoFS.setText("2");
        textFieldAnchoFS.setEnabled(false);
        textFieldAnchoFS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldAnchoFSActionPerformed(evt);
            }
        });
        getContentPane().add(textFieldAnchoFS, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 120, 30, 20));

        etiquetaAnchoFSActual.setText("Ancho FS");
        getContentPane().add(etiquetaAnchoFSActual, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 120, 60, 20));

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 10, 0, 690));

        etiquetaTextoBloqueosTotales.setText("Total Bloqueos:");
        getContentPane().add(etiquetaTextoBloqueosTotales, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 0, 90, 20));

        etiquetaBloqueosTotales.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        getContentPane().add(etiquetaBloqueosTotales, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 20, 50, 20));

        etiquetaTextoDemandasTotales.setText("Total Demandas:");
        getContentPane().add(etiquetaTextoDemandasTotales, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 0, 100, 20));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        etiquetaTextoMax.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        etiquetaTextoMax.setText("max");
        etiquetaTextoMax.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        etiquetaTextoMax.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        etiquetaTextoMax.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        jPanel1.add(etiquetaTextoMax, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 600, 30, 20));

        jTableResultadosBloqueosMinMax.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Entropía", "MSI", "BFR", "LightPaths", "PathConse", "Entr/uso", "% Uso", "Prob Bloq"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableResultadosBloqueosMinMax.setColumnSelectionAllowed(true);
        jScrollPane1.setViewportView(jTableResultadosBloqueosMinMax);
        jTableResultadosBloqueosMinMax.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 560, 340, 63));

        jTableResultadosBloqueos.setAutoCreateRowSorter(true);
        jTableResultadosBloqueos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tiempo", "Demandas", "Bloqueos", "Entropía", "MSI", "BFR", "LightPaths", "PathConse", "Entr/uso", "% Uso", "Prob Bloq"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableResultadosBloqueos.setColumnSelectionAllowed(true);
        jScrollPane3.setViewportView(jTableResultadosBloqueos);
        jTableResultadosBloqueos.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        jPanel1.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 30, 480, 520));

        etiquetaTextoMin.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        etiquetaTextoMin.setText("min");
        etiquetaTextoMin.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        etiquetaTextoMin.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        jPanel1.add(etiquetaTextoMin, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 590, 30, 20));

        etiquetaRSA1.setBackground(new java.awt.Color(255, 102, 102));
        etiquetaRSA1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        etiquetaRSA1.setText("Bloqueos");
        jPanel1.add(etiquetaRSA1, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 10, 80, -1));

        jTableResultados.setAutoCreateRowSorter(true);
        jTableResultados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tiempo", "Demandas", "Bloqueos", "Entropía", "MSI", "BFR", "LightPaths", "PathConse", "Entr/uso", "% Uso", "Prob Bloq"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableResultados.setColumnSelectionAllowed(true);
        jScrollPane4.setViewportView(jTableResultados);
        jTableResultados.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        jPanel1.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 30, 490, 520));

        etiquetaRSA3.setBackground(new java.awt.Color(255, 102, 102));
        etiquetaRSA3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        etiquetaRSA3.setText("Resultados");
        jPanel1.add(etiquetaRSA3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 80, -1));

        jTableResultadosMinMax.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Entropía", "MSI", "BFR", "LightPaths", "PathConse", "Entr/uso", "% Uso", "Prob Bloq"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableResultadosMinMax.setColumnSelectionAllowed(true);
        jScrollPane5.setViewportView(jTableResultadosMinMax);
        jTableResultadosMinMax.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        jPanel1.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 560, 350, 63));

        etiquetaTextoMin1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        etiquetaTextoMin1.setText("min");
        etiquetaTextoMin1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        etiquetaTextoMin1.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        jPanel1.add(etiquetaTextoMin1, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 590, 30, 20));

        etiquetaTextoMax1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        etiquetaTextoMax1.setText("max");
        etiquetaTextoMax1.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        etiquetaTextoMax1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        etiquetaTextoMax1.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        jPanel1.add(etiquetaTextoMax1, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 600, 30, 20));

        jTabbedPane1.addTab("Datos", jPanel1);

        panelResultados.setViewportView(filler1);

        jTabbedPane1.addTab("Gráficos", panelResultados);

        jTableEstadoEnlaces.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jTableEstadoEnlaces.setColumnSelectionAllowed(true);
        jScrollPane6.setViewportView(jTableEstadoEnlaces);

        jTabbedPane1.addTab("Estado Final de los Enlaces", jScrollPane6);

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTableResultadosDefrag.setAutoCreateRowSorter(true);
        jTableResultadosDefrag.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tiempo", "Rutas Activas", "Mejora %", "Mejor Hormiga/Generacion AG", "Rutas Modificadas"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Double.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane7.setViewportView(jTableResultadosDefrag);

        jPanel2.add(jScrollPane7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 360, 610));

        jTabbedPane1.addTab("Desfragmentaciones", jPanel2);

        getContentPane().add(jTabbedPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 40, 990, 670));

        etiquetaRSA2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        etiquetaRSA2.setText("Alg. de Ruteo");
        getContentPane().add(etiquetaRSA2, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 340, -1, -1));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel13.setText("Otros");
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 310, -1, -1));

        etiquetaAnchoFSActual19.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiquetaAnchoFSActual19.setText("0 = No considera");
        getContentPane().add(etiquetaAnchoFSActual19, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 690, 100, 20));

        textFieldPeriodoDesfrag.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        textFieldPeriodoDesfrag.setText("500");
        textFieldPeriodoDesfrag.setToolTipText("");
        textFieldPeriodoDesfrag.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldPeriodoDesfragActionPerformed(evt);
            }
        });
        getContentPane().add(textFieldPeriodoDesfrag, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 470, 40, 30));

        etiquetaAnchoFSActual22.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        etiquetaAnchoFSActual22.setText("Ordenar Demandas por UT.");
        getContentPane().add(etiquetaAnchoFSActual22, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 520, 150, 20));

        etiquetaTextoCantRutasReruteadas.setText("Total Rutas Reruteadas:");
        getContentPane().add(etiquetaTextoCantRutasReruteadas, new org.netbeans.lib.awtextra.AbsoluteConstraints(1130, 0, 140, 20));

        etiquetaCantRutasReruteadas.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        getContentPane().add(etiquetaCantRutasReruteadas, new org.netbeans.lib.awtextra.AbsoluteConstraints(1180, 20, 50, 20));

        etiquetaCantDesfrag.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        getContentPane().add(etiquetaCantDesfrag, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 20, 50, 20));

        etiquetaTextoCantDesfrag.setText("Cant. Desfragmentaciones:");
        getContentPane().add(etiquetaTextoCantDesfrag, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 0, -1, 20));

        ComboMetodoDesfrag.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "AG", "Peores Rutas", "ACO" }));
        ComboMetodoDesfrag.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboMetodoDesfragActionPerformed(evt);
            }
        });
        getContentPane().add(ComboMetodoDesfrag, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 440, 140, -1));

        ComboMetodo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DT Fijo", "Sin Desfragmentar", "Reactivo", "DT Variable Paper", "DT Variable Nuevo" }));
        ComboMetodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboMetodoActionPerformed(evt);
            }
        });
        getContentPane().add(ComboMetodo, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 432, 80, 30));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel9.setText("Método");
        jLabel9.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 430, -1, -1));

        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        etiquetaAnchoFSActual26.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiquetaAnchoFSActual26.setText("Rutas a Rerutear:");
        jPanel3.add(etiquetaAnchoFSActual26, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        textFieldRutasARerutear.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        textFieldRutasARerutear.setText("30");
        textFieldRutasARerutear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldRutasARerutearActionPerformed(evt);
            }
        });
        jPanel3.add(textFieldRutasARerutear, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 10, -1, -1));

        etiquetaAnchoFSActual12.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiquetaAnchoFSActual12.setText("%");
        jPanel3.add(etiquetaAnchoFSActual12, new org.netbeans.lib.awtextra.AbsoluteConstraints(228, 12, -1, -1));

        etiquetaTopologia2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        etiquetaTopologia2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiquetaTopologia2.setText("Objetivo Peores Rutas");
        jPanel3.add(etiquetaTopologia2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 36, -1, -1));

        ComboObjetivoReruteo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Entropía", "Path Consecutiveness", "BFR", "MSI" }));
        ComboObjetivoReruteo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboObjetivoReruteoActionPerformed(evt);
            }
        });
        jPanel3.add(ComboObjetivoReruteo, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 36, -1, -1));

        jTabbedPane2.addTab("Peores Rutas", jPanel3);
        jPanel3.getAccessibleContext().setAccessibleName("");

        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        etiquetaTopologia1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        etiquetaTopologia1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiquetaTopologia1.setText("Objetivo ACO");
        jPanel5.add(etiquetaTopologia1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        ComboObjetivoACO.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Entropía", "Path Consecutiveness", "BFR", "MSI" }));
        ComboObjetivoACO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboObjetivoACOActionPerformed(evt);
            }
        });
        jPanel5.add(ComboObjetivoACO, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 10, -1, -1));

        etiquetaAnchoFSActual8.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiquetaAnchoFSActual8.setText("Cant. hormigas:");
        jPanel5.add(etiquetaAnchoFSActual8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 36, -1, -1));

        textFieldCantHormigas.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        textFieldCantHormigas.setText("30");
        textFieldCantHormigas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldCantHormigasActionPerformed(evt);
            }
        });
        jPanel5.add(textFieldCantHormigas, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 36, -1, -1));

        etiquetaAnchoFSActual20.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiquetaAnchoFSActual20.setText("Mejora buscada en ACO:");
        jPanel5.add(etiquetaAnchoFSActual20, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 62, -1, -1));

        textFieldMejoraACO.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        textFieldMejoraACO.setText("20");
        textFieldMejoraACO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldMejoraACOActionPerformed(evt);
            }
        });
        jPanel5.add(textFieldMejoraACO, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 62, -1, -1));

        etiquetaAnchoFSActual6.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiquetaAnchoFSActual6.setText("%");
        jPanel5.add(etiquetaAnchoFSActual6, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 64, -1, -1));

        jTabbedPane2.addTab("ACO", jPanel5);

        jPanel7.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Objetivo");
        jPanel7.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        ComboObjAlgoritmoGenetico.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "MSI", "BFR" }));
        jPanel7.add(ComboObjAlgoritmoGenetico, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 10, -1, -1));

        jLabel7.setText("Cant. de individuos:");
        jPanel7.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(157, 10, -1, -1));

        jTextFieldCantIndividuosAG.setText("50");
        jTextFieldCantIndividuosAG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCantIndividuosAGActionPerformed(evt);
            }
        });
        jPanel7.add(jTextFieldCantIndividuosAG, new org.netbeans.lib.awtextra.AbsoluteConstraints(267, 10, 50, -1));

        label1.setForeground(new java.awt.Color(187, 187, 187));
        label1.setText("Cant.Generac.");
        jPanel7.add(label1, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 55, -1, -1));

        jTextFieldCantGeneraciones.setText("50");
        jTextFieldCantGeneraciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCantGeneracionesActionPerformed(evt);
            }
        });
        jPanel7.add(jTextFieldCantGeneraciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 55, 30, -1));

        jLabel8.setText("Tamaño del Cromosoma :");
        jPanel7.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, -1, -1));

        jTextFieldMejoraAG.setText("30");
        jTextFieldMejoraAG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldMejoraAGActionPerformed(evt);
            }
        });
        jPanel7.add(jTextFieldMejoraAG, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 30, 40, -1));

        etiquetaAnchoFSActual7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        etiquetaAnchoFSActual7.setText("% de las rutas activas");
        jPanel7.add(etiquetaAnchoFSActual7, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 30, -1, -1));

        jTextFieldProbBloqueo.setText("0.45");
        jPanel7.add(jTextFieldProbBloqueo, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 80, 70, -1));

        etiquetaProbBloqueo.setText("Parámetro");
        jPanel7.add(etiquetaProbBloqueo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, -1, -1));

        jTextFieldVentana.setText("50");
        jTextFieldVentana.setToolTipText("");
        jPanel7.add(jTextFieldVentana, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 80, -1, -1));

        etiquetaVentana.setText("Ventana");
        jPanel7.add(etiquetaVentana, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 80, -1, -1));

        ComboMetrica.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "BFR", "Entropia Shanon", "ABP" }));
        jPanel7.add(ComboMetrica, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 55, -1, -1));

        jLabel15.setText("Métrica");
        jPanel7.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 55, -1, -1));

        jTabbedPane2.addTab("AG", jPanel7);

        getContentPane().add(jTabbedPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 550, 390, 140));
        jTabbedPane2.getAccessibleContext().setAccessibleName("TabParametros");

        jLabel14.setText("Tipo de Trafico");
        getContentPane().add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, -1, -1));

        ComboTipoTrafico.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Fijo", "Variable" }));
        getContentPane().add(ComboTipoTrafico, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 370, -1, -1));

        jLabel16.setText("Cores");
        getContentPane().add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 120, 30, 20));

        textFieldCores.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        textFieldCores.setText("1");
        textFieldCores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textFieldCoresActionPerformed(evt);
            }
        });
        getContentPane().add(textFieldCores, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 120, 30, 20));

        checkDesfragFS.setText("Desfrag FS");
        getContentPane().add(checkDesfragFS, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 500, -1, -1));

        etiquetaAnchoFSActual23.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        etiquetaAnchoFSActual23.setText("Período Desfrag. para DT fijo");
        getContentPane().add(etiquetaAnchoFSActual23, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 470, 150, 20));

        ComboOrdenarDemandas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Dijkstra", "Ascendente", "Descendente" }));
        ComboOrdenarDemandas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboOrdenarDemandasActionPerformed(evt);
            }
        });
        getContentPane().add(ComboOrdenarDemandas, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 520, 60, -1));

        etiquetaError.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(etiquetaError, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 50, 220, 30));

        getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonEjecutarSimulacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEjecutarSimulacionActionPerformed

        // Evita desfragmentar cuando hay muchos bloqueos
        int noLogroEvitar = -1, yaDesfragmento = -1;

        this.etiquetaError.setText("Simulando...");
        this.etiquetaError.setVisible(true);

        //inicializamos algunas variables
        this.cantidadDeAlgoritmosTotalSeleccionados = 0;
        this.algoritmosCompletosParaGraficar.clear();

        //borramos los resultados que están en las tablas
        Utilitarios.reiniciarJTableRows(this.jTableResultados);
        Utilitarios.reiniciarJTableRows(this.jTableResultadosMinMax);
        Utilitarios.reiniciarJTableRows(this.jTableResultadosBloqueos);
        Utilitarios.reiniciarJTableRows(this.jTableResultadosBloqueosMinMax);
        Utilitarios.reiniciarJTableRows(this.jTableEstadoEnlaces);
        Utilitarios.reiniciarJTableColumns(this.jTableEstadoEnlaces);
        Utilitarios.reiniciarJTableRows(this.jTableResultadosDefrag);

        //pone en cero los resultados
        etiquetaCantRutasReruteadas.setText("0");
        etiquetaCantDesfrag.setText("0");
        etiquetaDemandasTotales.setText("0");
        etiquetaBloqueosTotales.setText("0");

        Integer resultadoReRuteo; //resultado Reruteo peores rutas

        //método
        this.metodo = (String) this.ComboMetodo.getSelectedItem();
        this.tipoTrafico = (String) this.ComboTipoTrafico.getSelectedItem();
        this.metrica = (String) this.ComboMetrica.getSelectedItem();

        //desfrag metodo
        String metodoDesfrag = (String) this.ComboMetodoDesfrag.getSelectedItem();

        //Peores rutas
        String ObjetivoReruteo = (String) this.ComboObjetivoReruteo.getSelectedItem();
        double porcRutasARerutear = Double.parseDouble(this.textFieldRutasARerutear.getText());

        //parámetros ACO
        double mejoraACO = Double.parseDouble(this.textFieldMejoraACO.getText());
        int cantHormACO = Integer.parseInt(this.textFieldCantHormigas.getText());
        String objetivoACO = (String) this.ComboObjetivoACO.getSelectedItem();

        // Parámetros AG
        int porcentajeLongCRAG = Integer.parseInt(this.jTextFieldMejoraAG.getText());
        int cantIndividuosAG = Integer.parseInt(this.jTextFieldCantIndividuosAG.getText());
        String objetivoAG = (String) this.ComboObjAlgoritmoGenetico.getSelectedItem();
        int cantGeneracionesAG = Integer.parseInt(this.jTextFieldCantGeneraciones.getText());
        // Leemos los valores seteados
        this.tiempoTotal = Integer.parseInt(this.spinnerTiempoSimulacion.getValue().toString()); //Tiempo de simulacion indicado por el usuario
        this.redSeleccionada = (String) this.listaRedes.getSelectedItem(); // obtenemos la topologia seleccionada en letras

        this.anchoFS = Double.parseDouble(this.textFieldAnchoFS.getText()); // ancho de los FSs de la toplogia elegida, tambien indicado por el usuario
        this.capacidadPorEnlace = Integer.parseInt(this.textFieldCapacidadEnlace.getText()); //obtenemos la cantidad de FS de los enlaces indicados por el usuario
        this.Erlang = Integer.parseInt(this.spinnerErlang.getValue().toString()); //obtenemos Erlang indicados por el usuario
        this.Lambda = Integer.parseInt(this.textFieldLambda.getText()); //obtenemos Erlang indicados por el usuario
        this.HoldingTime = (Erlang / Lambda); // Erlang / Lambda
        this.FsMinimo = Integer.parseInt(this.textFieldFSminimo.getText()); //obtenemos FSminimo indicados por el usuario
        this.FsMaximo = Integer.parseInt(this.textFieldFSmaximo.getText()); //obtenemos FSmaximo indicados por el usuario

        /*Declaracion de estructura de datos para estadisticas de bloqueo */
        /*vector para guardar los datos y establecer una estadistica de bloqueos 
        por cantidad de ranuras requeridas*/
        int tam = FsMaximo - FsMinimo + 2;
        int ranuras[] = new int[tam];/*se toma el indice del vector como la cantidad de ranura
         en ella se guardan la suma de los bloqueos para cada ranura */
        int cantidadTotalBloqueos = 0;
        int cantBloqueosPaper = 0;
        /*fin de la declaracion para datos estadisticos de bloqueo*/

        //FS mínimos para considerar en el PatchConsecutiveness
        int FSMinPC = (int) (FsMaximo - ((FsMaximo - FsMinimo) * 0.3));

        //Guardar el seleccionado en la lista de algoritmos seleccionados, más adelante ver como agregar más algoritmos a la lista
        List algoritmosRuteoSeleccionados = this.listaAlgoritmosRuteo.getSelectedValuesList();
        String algoritmoSeleccionado = (String) algoritmosRuteoSeleccionados.get(0);
        //System.out.println("El algoritmosRuteoSeleccionados22:"+algoritmoSeleccionado);
        this.algoritmosCompletosParaGraficar.add(cantidadDeAlgoritmosTotalSeleccionados, algoritmoSeleccionado);
        this.cantidadDeAlgoritmosTotalSeleccionados++;

        //parámetros desfragmentación
        int periodoDesfrag = Integer.parseInt(this.textFieldPeriodoDesfrag.getText()); //Tiempo de simulacion indicado por el usuario
        int ultimoDesfrag = periodoDesfrag;

        // Cores
        this.cores = Integer.parseInt(this.textFieldCores.getText());

        this.desfragmentarFS = this.checkDesfragFS.isSelected();
        this.ordenarDemandas = (String) this.ComboOrdenarDemandas.getSelectedItem();

        GrafoMatriz G[][] = new GrafoMatriz[this.cores][this.algoritmosCompletosParaGraficar.size()]; // Se tiene una matriz de adyacencia por algoritmo RSA elegidos para por el usuario
        ArrayList<ArrayList<ListaEnlazada>> caminosDeDosEnlaces = new ArrayList<>();
        rutasEstablecidas = new ArrayList(this.cores);
        arrayRutas = new ArrayList<>(this.cores);
        resultadoRuteo = new ArrayList<>(this.cores);
        listaKSP = new ArrayList<>(this.cores);

        for (int core = 0; core < this.cores; core++) {
            caminosDeDosEnlaces.add(null);
            rutasEstablecidas.add(new ArrayList<>());
            arrayRutas.add(new ArrayList<>());
            resultadoRuteo.add(new ArrayList<>());
            listaKSP.add(new ArrayList<>());
        }

        File archivoDemandas = null;
        Resultado r; // resultado obtenido en una demanda. Si r==null se cuenta como bloqueo
        String mensajeError = "Seleccione: "; // mensaje de error a mostrar eal usuario si no ha completado los parametros de
        // simulacion

        List<String> RSA; // lista de Algoritmos RSA seleccionados

        ArrayList<Demanda> demandasPorUnidadTiempo = new ArrayList<>(); //ArrayList que contiene las demandas para una unidad de tiempo T

        int k = -1; // contador auxiliar
        //int paso = (int) this.spinnerPaso.getValue(); // siguiente carga de trafico a simular (Erlang)
        int contD = 0; // contador de demandas totales
        int tiempoT = Integer.parseInt(this.spinnerTiempoSimulacion.getValue().toString()); // Tiempo de simulacion especificada por el usaurio
        anchoFS = Double.parseDouble(this.textFieldAnchoFS.getText()); // ancho de FS
        probBloqueoBth = Double.parseDouble(this.jTextFieldProbBloqueo.getText()); // ancho de FS
        //System.err.println("Simulacion con probabilidad "+proba450Erlang[gigante]);
        this.ventanaPar = Double.parseDouble(this.jTextFieldVentana.getText()); // ventana

        System.out.println(this.probBloqueoBth);
        //factor del tiempo de simulacion especificado por el usuario

        System.out.println("El ancho del FS es:" + anchoFS);
        System.out.println("Cantidad de FS por enlace:" + capacidadPorEnlace);
        System.out.println("Cantidad Algoritmos:" + this.cantidadDeAlgoritmosTotalSeleccionados);
        System.out.println("Cantidad Cores:" + this.cores);

        //if(this.listaDemandas.getSelectedIndex()>=0 && this.listaAlgoritmosRuteo.getSelectedIndex()>=0 && 
        //        this.listaRedes.getSelectedIndex()>=0 && this.listaAlgoritmosAS.getSelectedIndex()>=0 && this.cantidadDeAlgoritmosTotalSeleccionados >0){ // si todos los parametros fueron seleccionados
        if (this.listaAlgoritmosRuteo.getSelectedIndex() >= 0 && this.listaRedes.getSelectedIndex() >= 0 && this.cantidadDeAlgoritmosTotalSeleccionados > 0) {

            RSA = this.algoritmosCompletosParaGraficar; // obtenemos los algoritmos RSA seleccionados

            //String demandaSeleccionada = this.listaDemandas.getSelectedValue(); // obtenemos el tipo de trafico seleccionado
            int[] conexid = new int[RSA.size()];

            for (int i = 0; i < conexid.length; i++) {
                conexid[i] = 0;
            }

            int[] contB = new int[RSA.size()]; // array encargado de almacenar en cada posicion la cantidad de bloqueo para cada
            // algoritmo seleccionado
            List prob[] = new LinkedList[RSA.size()]; // probabilidad de bloqueo para cada algoritmo RSA selecciondo 

            for (int i = 0; i < prob.length; i++) {
                prob[i] = new LinkedList(); // para cada algoritmo, se tiene una lista enlazada que almacenara la Pb 
                // obtenidad en cada simulacion
            }

            // Inicialización por core
            for (int j = 0; j < this.cores; j++) {
                switch (redSeleccionada) { // cargamos los datos en las matrices de adyacencia segun la topologia seleccionada
                    case "Red 0":
                        topologia = this.Redes.getTopologia(0);
                        //de ´rueba no utilizado
                        for (int i = 0; i < RSA.size(); i++) {
                            G[j][i] = new GrafoMatriz(this.Redes.getRed(0).getCantidadDeVertices());
                            G[j][i].insertarDatos(topologia);
                        }
                        break;
                    case "NSFNet":
                        topologia = this.Redes.getTopologia(1);
                        for (int i = 0; i < RSA.size(); i++) {
                            G[j][i] = new GrafoMatriz(this.Redes.getRed(1).getCantidadDeVertices());
                            G[j][i].insertarDatos(topologia);
                        }
                        caminosDeDosEnlaces.set(j, Utilitarios.hallarCaminosTomadosDeADos(topologia, 14, 21));
                        break;
                    case "ARPA-2":
                        topologia = this.Redes.getTopologia(2);
                        for (int i = 0; i < RSA.size(); i++) {
                            G[j][i] = new GrafoMatriz(this.Redes.getRed(2).getCantidadDeVertices());
                            G[j][i].insertarDatos(topologia);
                        }
                        caminosDeDosEnlaces.set(j, Utilitarios.hallarCaminosTomadosDeADos(topologia, 21, 26));
                    case "USNet":
                        topologia = this.Redes.getTopologia(3);
                        for (int i = 0; i < RSA.size(); i++) {
                            G[j][i] = new GrafoMatriz(this.Redes.getRed(3).getCantidadDeVertices());
                            G[j][i].insertarDatos(topologia);
                        }
                        caminosDeDosEnlaces.set(j, Utilitarios.hallarCaminosTomadosDeADos(topologia, 24, 43));
                }
            }

            //generar archivo de demandas
            try {
                //while (earlang <= E) { // mientras no se llega a la cargad de trafico maxima
                archivoDemandas = Utilitarios.generarArchivoDemandas(Lambda, tiempoTotal, FsMinimo, FsMaximo, G[0][0].getCantidadDeVertices(), HoldingTime, Erlang, this.tipoTrafico);
            } catch (IOException ex) {
                Logger.getLogger(VentanaPrincipal_Defrag_ProAct.class.getName()).log(Level.SEVERE, null, ex);
            }
            //Construimos el nombre del archivo con la fecha y hora
            Calendar calendario = new GregorianCalendar();
            hora = calendario.get(Calendar.HOUR_OF_DAY);
            minutos = calendario.get(Calendar.MINUTE);
            segundos = calendario.get(Calendar.SECOND);
            dia = calendario.get(Calendar.DAY_OF_MONTH);
            mes = calendario.get(Calendar.MONTH);
            anho = calendario.get(Calendar.YEAR);
            File carpeta = new File(System.getProperty("user.dir") + "\\src\\Defrag\\ProAct\\Archivos\\Resultados\\");
            String detallesNombre = "" + Lambda + "k_" + tiempoTotal + "t-" + RSA.get(0) + "-" + dia + "-" + mes + "-" + anho + "-" + hora + "_" + minutos + "_" + segundos + ".txt";
            String ruta = System.getProperty("user.dir") + "\\src\\Defrag\\ProAct\\Archivos\\Resultados\\Resultado" + detallesNombre;
            String rutaDefrag = System.getProperty("user.dir") + "\\src\\Defrag\\ProAct\\Archivos\\Resultados\\Defrag" + detallesNombre;
            String rutaEstados = System.getProperty("user.dir") + "\\src\\Defrag\\ProAct\\Archivos\\Resultados\\Estados" + detallesNombre;
            if (!carpeta.exists()) {
                carpeta.mkdirs();
            }
            File archivoResultados = new File(ruta);
            File archivoDefrag = new File(rutaDefrag);
            File archivoEstados = new File(rutaEstados);

            int sumaTiempoDeVida = 0, ultDef = 0;
            int deltatiempo = 0;

            String algoritmoAejecutar = RSA.get(0);

            for (int i = 1; i <= tiempoT; i++) {
                System.out.println("Tiempo: " + i);

                try {
                    demandasPorUnidadTiempo = Utilitarios.leerDemandasPorTiempo(archivoDemandas, i); //lee las demandas para el tiempo i
                    /*   System.out.println("DEMANDAS EN EL TIEMPO:" + i + " SIN ORDENAR SON:");
                    for (int n = 0; n < demandasPorUnidadTiempo.size(); n++) {
                        System.out.println(demandasPorUnidadTiempo.get(n).getTiempo());
                    }*/
                    if (!demandasPorUnidadTiempo.isEmpty()) {
                        switch (ordenarDemandas) {
                            case "Ascendente":
                                quicksortDemandas(demandasPorUnidadTiempo, 0, (demandasPorUnidadTiempo.size() - 1));
                                break;
                            case "Descendente":
                                quicksortDemandas(demandasPorUnidadTiempo, 0, (demandasPorUnidadTiempo.size() - 1));
                                quickSortDemandasDescendente(demandasPorUnidadTiempo);            //Si preferimos ordenar de manera descendente
                                break;
                            case "Dijkstra":
                                ListaEnlazada[] dijkstra = new ListaEnlazada[demandasPorUnidadTiempo.size()];
                                int count = 0;
                                //System.out.println("DIJKSTRA - Antes de ordenar: ");
                                for (Demanda demanda : demandasPorUnidadTiempo) {
                                    dijkstra[count] = Dijkstra(G[0][0], demanda.getOrigen(), demanda.getDestino());
                                    dijkstra[count].setDemandIndex(count);
                                    //System.out.println(dijkstra[count].getTamanho());
                                    count++;
                                }
                                quicksortDijkstra(dijkstra, 0, (dijkstra.length - 1));
                                ArrayList<Demanda> aux = new ArrayList<>();
                                //System.out.println("DIJKSTRA - Despúes de ordenar: ");
                                for (int n = 0; n < (dijkstra.length - 1); n++) {
                                    aux.add(demandasPorUnidadTiempo.get(dijkstra[n].getDemandIndex()));
                                    //System.out.println(dijkstra[n].getTamanho());
                                }
                                demandasPorUnidadTiempo = aux;
                        }
                    }
                    /* System.out.println("DEMANDAS EN EL TIEMPO:" + i + " TRAS ORDENAR SON:");
                    for (int n = 0; n < demandasPorUnidadTiempo.size(); n++) {
                        System.out.println(demandasPorUnidadTiempo.get(n).getTiempo());
                    }*/
                } catch (IOException ex) {
                    Logger.getLogger(VentanaPrincipal_Defrag_ProAct.class.getName()).log(Level.SEVERE, null, ex);
                }
                for (Demanda demanda : demandasPorUnidadTiempo) { // para cada demanda
                    /*Boolean[] tested = new Boolean[this.cores];
                    for (int core = 0; core < this.cores; core++) {
                        tested[core] = false;
                    }
                    int coreDemanda = Utilitarios.getCore(this.cores, tested);*/
                    sumaTiempoDeVida = sumaTiempoDeVida + demanda.getTiempo();
                    esBloqueo = false;
                    int intentos = 0;
                    boolean asignado = false;
                    while (!esBloqueo && intentos < this.cores && !asignado) {
                        ArrayList<ListaEnlazada> kspAux = new ArrayList<>();
                        for (int core = 0; core < this.cores; core++) {
                            ListaEnlazada[] kspUnCore = Utilitarios.KSP(G[core][0], demanda.getOrigen(), demanda.getDestino(), 5); // calculamos los k caminos mas cortos entre el origen y el fin. Con k=5 (pude ser mas, cambiar dependiendo de la necesidad)
                            for (ListaEnlazada kspItem : kspUnCore) {
                                kspItem.setCore(core);
                                kspAux.add(kspItem);
                            }
                        }
                        ListaEnlazada[] ksp = kspAux.toArray(new ListaEnlazada[0]);
                        int coreDemanda;
                        for (int a = 0; a < RSA.size(); a++) {

                            algoritmoAejecutar = RSA.get(a);

                            switch (algoritmoAejecutar) {
                                case "FA":
                                    r = Algoritmos_Defrag_ProAct.Def_FA(G, demanda, ksp, capacidadPorEnlace, a);
                                    if (r != null) {//si se pudo establecer la demanda
                                        coreDemanda = ksp[r.getCamino()].getCore();
                                        Utilitarios.asignarFS_Defrag(ksp, r, G[coreDemanda][a], demanda, ++conexid[a]);
                                        rutasEstablecidas.get(coreDemanda).add(demanda.getTiempo());
                                        arrayRutas.get(coreDemanda).add(ksp[r.getCamino()]);
                                        resultadoRuteo.get(coreDemanda).add(r);
                                        listaKSP.get(coreDemanda).add(ksp);
                                        asignado = true;
                                    } else {
                                        /*Inicio de creacion de estadistica de los bloqueos segun la cantidad de ranuras requeridas*/

                                        int cantidadRanurasRequeridas = demanda.getNroFS();
                                        ranuras[cantidadRanurasRequeridas] += 1;
                                        cantidadTotalBloqueos++;
                                        cantBloqueosPaper++;
                                        System.out.println("Cant Bloqueos: " + cantBloqueosPaper);

                                        /*Fin del bloque para obtener los datos estadisticos de bloqueo*/
                                        if (metodo.equals("Reactivo") && noLogroEvitar < i && yaDesfragmento != i) {
                                            System.out.println("Inicia desfragmentacion en el tiempo " + i + " con " + arrayRutas.size() + " rutas activas");
                                            yaDesfragmento = i;
                                            try {
                                                for (int coreAux = 0; coreAux < this.cores; coreAux++) {
                                                    switch (metodoDesfrag) {
                                                        case "ACO":
                                                            encontroSolucion = Utilitarios.desfragmentacionACO(topologia, RSA.get(0), resultadoRuteo.get(coreAux), arrayRutas.get(coreAux), mejoraACO, capacidadPorEnlace, G[coreAux][a], listaKSP.get(coreAux), archivoDefrag, i, cantHormACO, caminosDeDosEnlaces.get(coreAux), this.jTableEstadoEnlaces, FSMinPC, objetivoACO, coreAux);
                                                            if (encontroSolucion) {
                                                                if (desfragmentarFS) {
                                                                    Algoritmos_Defrag_ProAct.defragmentarFS(G[coreAux][0], capacidadPorEnlace);
                                                                }
                                                                coreAux = this.cores;
                                                            }
                                                            break;
                                                        case "AG":
                                                            encontroSolucionAG = Utilitarios.desfragmentacionAG(topologia, RSA.get(0), resultadoRuteo.get(coreAux), arrayRutas.get(coreAux), porcentajeLongCRAG, capacidadPorEnlace, G[coreAux][a], listaKSP.get(coreAux), archivoDefrag, i, cantIndividuosAG, objetivoAG, cantGeneracionesAG, coreAux);
                                                            if (encontroSolucionAG) {
                                                                if (desfragmentarFS) {
                                                                    Algoritmos_Defrag_ProAct.defragmentarFS(G[coreAux][0], capacidadPorEnlace);
                                                                }
                                                                coreAux = this.cores;
                                                            }
                                                            break;
                                                        default:
                                                            resultadoReRuteo = Utilitarios.desfragmentacionPeoresRutas(topologia, G[coreAux][a], capacidadPorEnlace, arrayRutas.get(coreAux), resultadoRuteo.get(coreAux), listaKSP.get(coreAux), ObjetivoReruteo, porcRutasARerutear, FSMinPC, algoritmoAejecutar, rutasEstablecidas.get(coreAux), coreAux);
                                                            //suma el resultado
                                                            etiquetaCantRutasReruteadas.setText("" + (int) (Integer.parseInt("" + etiquetaCantRutasReruteadas.getText()) + resultadoReRuteo));
                                                            if (resultadoReRuteo > 0) { //si resultadoReRuteo es mayor a cero si se hizo la desfragmentación
                                                                if (desfragmentarFS) {
                                                                    Algoritmos_Defrag_ProAct.defragmentarFS(G[coreAux][0], capacidadPorEnlace);
                                                                }
                                                                coreAux = this.cores;
                                                                etiquetaCantDesfrag.setText("" + (int) (Integer.parseInt("" + etiquetaCantDesfrag.getText()) + 1)); //suma 1
                                                            }
                                                            break;
                                                    }
                                                }
                                            } catch (IOException ex) {
                                                Logger.getLogger(VentanaPrincipal_Defrag_ProAct.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            r = Algoritmos_Defrag_ProAct.Def_FA(G, demanda, ksp, capacidadPorEnlace, a);
                                            if (r != null) {
                                                coreDemanda = ksp[r.getCamino()].getCore();
                                                Utilitarios.asignarFS_Defrag(ksp, r, G[coreDemanda][a], demanda, ++conexid[a]);
                                                rutasEstablecidas.get(coreDemanda).add(demanda.getTiempo());
                                                arrayRutas.get(coreDemanda).add(ksp[r.getCamino()]);
                                                resultadoRuteo.get(coreDemanda).add(r);
                                                listaKSP.get(coreDemanda).add(ksp);
                                                asignado = true;
                                            } else {
                                                System.out.println("Desfragmento en el tiempo: " + i + "pero no logro evitar el bloqueo");
                                                noLogroEvitar = i;
                                                contB[a]++;
                                                contBloqueos++;
                                                esBloqueo = true;
                                            }
                                        } else {
                                            contB[a]++;
                                            contBloqueos++;
                                            esBloqueo = true;
                                        }
                                    }
                                    break;
                                case "FA-CA":
                                    r = Algoritmos_Defrag_ProAct.Def_FACA(G, demanda, ksp, capacidadPorEnlace, a);
                                    if (r != null) {
                                        coreDemanda = ksp[r.getCamino()].getCore();
                                        Utilitarios.asignarFS_Defrag(ksp, r, G[coreDemanda][a], demanda, ++conexid[a]);
                                        rutasEstablecidas.get(coreDemanda).add(demanda.getTiempo());
                                        arrayRutas.get(coreDemanda).add(ksp[r.getCamino()]);
                                        resultadoRuteo.get(coreDemanda).add(r);
                                        listaKSP.get(coreDemanda).add(ksp);
                                        asignado = true;
                                    } else {
                                        if (metodo.equals("Reactivo") && noLogroEvitar < i) {
                                            try {
                                                switch (metodoDesfrag) {
                                                    case "ACO":
                                                        for (int core = 0; core < this.cores; core++) {
                                                            encontroSolucion = Utilitarios.desfragmentacionACO(topologia, RSA.get(0), resultadoRuteo.get(core), arrayRutas.get(core), mejoraACO, capacidadPorEnlace, G[core][a], listaKSP.get(core), archivoDefrag, i, cantHormACO, caminosDeDosEnlaces.get(core), this.jTableEstadoEnlaces, FSMinPC, objetivoACO, core);
                                                            if (encontroSolucion) {
                                                                break;
                                                            }
                                                        }
                                                        break;
                                                    case "AG":
                                                        for (int core = 0; core < this.cores; core++) {
                                                            encontroSolucionAG = Utilitarios.desfragmentacionAG(topologia, RSA.get(0), resultadoRuteo.get(core), arrayRutas.get(core), porcentajeLongCRAG, capacidadPorEnlace, G[core][a], listaKSP.get(core), archivoDefrag, i, cantIndividuosAG, objetivoAG, cantGeneracionesAG, core);
                                                            if (encontroSolucionAG) {
                                                                break;
                                                            }
                                                        }
                                                        break;
                                                    default:
                                                        for (int core = 0; core < this.cores; core++) {
                                                            resultadoReRuteo = Utilitarios.desfragmentacionPeoresRutas(topologia, G[core][a], capacidadPorEnlace, arrayRutas.get(core), resultadoRuteo.get(core), listaKSP.get(core), ObjetivoReruteo, porcRutasARerutear, FSMinPC, algoritmoAejecutar, rutasEstablecidas.get(core), core);
                                                            //suma el resultado
                                                            etiquetaCantRutasReruteadas.setText("" + (int) (Integer.parseInt("" + etiquetaCantRutasReruteadas.getText()) + resultadoReRuteo));
                                                            if (resultadoReRuteo > 0) { //si resultadoReRuteo es mayor a cero si se hizo la desfragmentación
                                                                etiquetaCantDesfrag.setText("" + (int) (Integer.parseInt("" + etiquetaCantDesfrag.getText()) + 1)); //suma 1
                                                                break;
                                                            }
                                                        }
                                                        break;
                                                }
                                            } catch (IOException ex) {
                                                Logger.getLogger(VentanaPrincipal_Defrag_ProAct.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            r = Algoritmos_Defrag_ProAct.Def_FACA(G, demanda, ksp, capacidadPorEnlace, a);
                                            if (r != null) {
                                                coreDemanda = ksp[r.getCamino()].getCore();
                                                Utilitarios.asignarFS_Defrag(ksp, r, G[coreDemanda][a], demanda, ++conexid[a]);
                                                rutasEstablecidas.get(coreDemanda).add(demanda.getTiempo());
                                                arrayRutas.get(coreDemanda).add(ksp[r.getCamino()]);
                                                resultadoRuteo.get(coreDemanda).add(r);
                                                listaKSP.get(coreDemanda).add(ksp);
                                                asignado = true;
                                            } else {
                                                System.out.println("Desfragmento en el tiempo: " + i + "pero no logro evitar e bloqueo");
                                                noLogroEvitar = i;
                                                contB[a]++;
                                                contBloqueos++;
                                                esBloqueo = true;
                                            }
                                        } else {
                                            contB[a]++;
                                            contBloqueos++;
                                            esBloqueo = true;
                                        }
                                    }
                                    break;
                                case "MTLSC":
                                    r = Algoritmos.MTLSC_Algorithm(G, demanda, ksp, capacidadPorEnlace, a);
                                    if (r != null) {
                                        coreDemanda = ksp[r.getCamino()].getCore();
                                        Utilitarios.asignarFS_Defrag(ksp, r, G[coreDemanda][a], demanda, ++conexid[a]);
                                        rutasEstablecidas.get(coreDemanda).add(demanda.getTiempo());
                                        arrayRutas.get(coreDemanda).add(ksp[r.getCamino()]);
                                        resultadoRuteo.get(coreDemanda).add(r);
                                        asignado = true;
                                    } else {
                                        contB[a]++;
                                        contBloqueos++;
                                        esBloqueo = true;
                                    }
                                    break;
                            }

                        }
                    }
                    contD++;
                    //Para cada demanda guardar el estado de la red, para el analisisde metricas
                    for (int a = 0; a < RSA.size(); a++) {
                        for (int core = 0; core < this.cores; core++) {
                            //Escribimos el archivo de resultados
                            entropia = msi = bfr = pathConsec = entropiaUso = 0.0;
                            entropia = G[core][a].entropia();
                            msi = Metricas.MSI(G[core][a], capacidadPorEnlace);
                            bfr = Metricas.BFR(G[core][a], capacidadPorEnlace);
                            pathConsec = Metricas.PathConsecutiveness(caminosDeDosEnlaces.get(core), capacidadPorEnlace, G[core][a], FSMinPC);
                            entropiaUso = Metricas.EntropiaPorUso(caminosDeDosEnlaces.get(core), capacidadPorEnlace, G[core][a]);
                            porcUso = Metricas.PorcUsoGrafo(G[core][a]);
                            Utilitarios.escribirArchivoEstados(archivoEstados, entropia, msi, bfr, pathConsec, entropiaUso, esBloqueo, rutasEstablecidas.get(core).size(), porcUso, core);
                        }
                    }

                }

                for (int core = 0; core < this.cores; core++) {
                    //Disminuir el tiempo de vida de todas las rutas en la red
                    for (int j = 0; j < RSA.size(); j++) {
                        Utilitarios.Disminuir(G[core][j]);
                    }

                    //verificar si la ruta sigue activa o no dentro de la red.
                    for (int index = 0; index < rutasEstablecidas.get(core).size(); index++) {
                        rutasEstablecidas.get(core).set(index, rutasEstablecidas.get(core).get(index) - 1);
                    }
                    //Segundo for para evitar problemas con los indices al borrar
                    for (int index = 0; index < rutasEstablecidas.get(core).size(); index++) {
                        if (rutasEstablecidas.get(core).get(index) == 0) { //si el tiempo de vida es cero
                            rutasEstablecidas.get(core).remove(index); //remover del contador de rutas establecidas
                            arrayRutas.get(core).remove(index); //remover la ruta de la lista de rutas vigentes
                            resultadoRuteo.get(core).remove(index);//remueve de la lista de resultados de ruteo
                            listaKSP.get(core).remove(index);
                            index--;
                        }
                    }
                    for (int a = 0; a < RSA.size(); a++) {
                        //Escribimos el archivo de resultados
                        entropia = msi = bfr = pathConsec = entropiaUso = probBloqueo = 0.0;
                        entropia = G[core][a].entropia();
                        msi = Metricas.MSI(G[core][a], capacidadPorEnlace);
                        bfr = Metricas.BFR(G[core][a], capacidadPorEnlace);
                        bjj678 = Metricas.BJJ678(G[core][a], capacidadPorEnlace);
                        shanonEntropy = G[core][a].entropiaShanon(capacidadPorEnlace);
                        abpgrafo = G[core][a].ABP(capacidadPorEnlace);
                        pathConsec = Metricas.PathConsecutiveness(caminosDeDosEnlaces.get(core), capacidadPorEnlace, G[core][a], FSMinPC);
                        entropiaUso = Metricas.EntropiaPorUso(caminosDeDosEnlaces.get(core), capacidadPorEnlace, G[core][a]);
                        porcUso = Metricas.PorcUsoGrafo(G[core][a]);
                        probBloqueo = Utilitarios.calcularProbabilidadDeBloqueo(entropia, msi, bfr, pathConsec, entropiaUso, porcUso, arrayRutas.get(core).size());
                        Utilitarios.escribirArchivoResultados(archivoResultados, i, contBloqueos, demandasPorUnidadTiempo.size(), entropia, msi/*bjj678*/ /*shanonEntropy*/, bfr, rutasEstablecidas.get(core).size(), pathConsec /*abpgrafo*/, entropiaUso, porcUso, probBloqueo, core);
                    }

                    //no modificar
                    switch (metodo) {
                        case "DT Fijo":
                            if (i == ultimoDesfrag && i != tiempoTotal) {// cada periodo y que no haga si es el ultimo tiempo
                                ultimoDesfrag = ultimoDesfrag + periodoDesfrag;
                                try {
                                    System.out.println("Inicia desfragmentacion en tiempo " + i + " con " + arrayRutas.get(core).size() + " rutas activas en el core " + core);
                                    switch (metodoDesfrag) {
                                        case "ACO":
                                            encontroSolucion = Utilitarios.desfragmentacionACO(topologia, RSA.get(0), resultadoRuteo.get(core), arrayRutas.get(core), mejoraACO, capacidadPorEnlace, G[core][0], listaKSP.get(core), archivoDefrag, i, cantHormACO, caminosDeDosEnlaces.get(core), this.jTableEstadoEnlaces, FSMinPC, objetivoACO, core);
                                            break;
                                        case "AG":
                                            encontroSolucionAG = Utilitarios.desfragmentacionAG(topologia, RSA.get(0), resultadoRuteo.get(core), arrayRutas.get(core), porcentajeLongCRAG, capacidadPorEnlace, G[core][0], listaKSP.get(core), archivoDefrag, i, cantIndividuosAG, objetivoAG, cantGeneracionesAG, core);
                                            break;
                                        default:
                                            resultadoReRuteo = Utilitarios.desfragmentacionPeoresRutas(topologia, G[core][0], capacidadPorEnlace, arrayRutas.get(core), resultadoRuteo.get(core), listaKSP.get(core), ObjetivoReruteo, porcRutasARerutear, FSMinPC, algoritmoAejecutar, rutasEstablecidas.get(core), core);
                                            //suma el resultado
                                            etiquetaCantRutasReruteadas.setText("" + (int) (Integer.parseInt("" + etiquetaCantRutasReruteadas.getText()) + resultadoReRuteo));
                                            if (resultadoReRuteo > 0) { //si resultadoReRuteo es mayor a cero si se hizo la desfragmentación
                                                etiquetaCantDesfrag.setText("" + (int) (Integer.parseInt("" + etiquetaCantDesfrag.getText()) + 1)); //suma 1
                                            }
                                            break;
                                    }
                                    if (desfragmentarFS) {
                                        Algoritmos_Defrag_ProAct.defragmentarFS(G[core][0], capacidadPorEnlace, resultadoRuteo.get(core), rutasEstablecidas.get(core), arrayRutas.get(core));
                                    }
                                } catch (IOException ex) {
                                    Logger.getLogger(VentanaPrincipal_Defrag_ProAct.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            break;
                        case "DT Variable Nuevo": {
                            //ultimoDesfrag = ultimoDesfrag + periodoDesfrag;
                            double fragIndex = 0;
                            switch (this.metrica) {
                                case "Entropia Shanon":
                                    fragIndex = G[core][0].entropiaShanon(capacidadPorEnlace);
                                    System.out.println("Entropia Shanon: " + fragIndex);
                                    break;
                                case "ABP":
                                    fragIndex = G[core][0].ABP(capacidadPorEnlace);
                                    System.out.println("ABP: " + fragIndex);
                                    //fragIndex = Metricas.ABP(G[0],capacidadPorEnlace);
                                    break;
                                case "BFR":
                                    fragIndex = Metricas.BFR(G[core][0], capacidadPorEnlace);
                                    break;
                                default:
                                    break;
                            }
                            //fragIndex = Metricas.BJJ678(G[0],capacidadPorEnlace);
                            //fragIndex = Metricas.BFR(G[0],capacidadPorEnlace);
                            //fragIndex = Metricas.MSI(G[0],capacidadPorEnlace);
                            //fragIndex = G[0].entropia();
                            //System.out.println("BFR T VARIABLE: " + bfrGrafo);
                            //System.out.println("ultimo desfrag: "+ultimoDesfrag);
                            //if((i >= (ultimoDesfrag-20) && i <= (ultimoDesfrag+20))  && (fragIndex > this.probBloqueoBth ||  i == (ultimoDesfrag+20))  && i != tiempoTotal){// cada periodo y que no haga si es el ultimo tiempo
                            //con erlang 800 sin des: 251 con DT Vstisble:162 y 27 defrag, 186 fijo
                            // 600 erlang sin des: 46 dt variable 21 b 9 defrag - dtfijo 36 b 9 defrag
                            // 650 erlang sin des: 67 / dt variable 46 b 12 defrag - dtfijo 55
                            if ((ultDef + this.ventanaPar) <= i) {
                                if (fragIndex > this.probBloqueoBth && i != tiempoTotal) {// cada periodo y que no haga si es el ultimo tiempo
                                    //System.out.println("ultimo desfrag dentro: "+ultimoDesfrag);
                                    ultDef = i;
                                    ultimoDesfrag = ultimoDesfrag + periodoDesfrag;
                                    try {
                                        System.out.println("Inicia desfragmentacion en tiempo " + i + " con " + arrayRutas.size() + " rutas activas");
                                        switch (metodoDesfrag) {
                                            case "ACO":
                                                encontroSolucion = Utilitarios.desfragmentacionACO(topologia, RSA.get(0), resultadoRuteo.get(core), arrayRutas.get(core), mejoraACO, capacidadPorEnlace, G[core][0], listaKSP.get(core), archivoDefrag, i, cantHormACO, caminosDeDosEnlaces.get(core), this.jTableEstadoEnlaces, FSMinPC, objetivoACO, core);
                                                break;
                                            case "AG":
                                                encontroSolucionAG = Utilitarios.desfragmentacionAG(topologia, RSA.get(0), resultadoRuteo.get(core), arrayRutas.get(core), porcentajeLongCRAG, capacidadPorEnlace, G[core][0], listaKSP.get(core), archivoDefrag, i, cantIndividuosAG, objetivoAG, cantGeneracionesAG, core);
                                                break;
                                            default:
                                                resultadoReRuteo = Utilitarios.desfragmentacionPeoresRutas(topologia, G[core][0], capacidadPorEnlace, arrayRutas.get(core), resultadoRuteo.get(core), listaKSP.get(core), ObjetivoReruteo, porcRutasARerutear, FSMinPC, algoritmoAejecutar, rutasEstablecidas.get(core), core);
                                                //suma el resultado
                                                etiquetaCantRutasReruteadas.setText("" + (int) (Integer.parseInt("" + etiquetaCantRutasReruteadas.getText()) + resultadoReRuteo));
                                                if (resultadoReRuteo > 0) { //si resultadoReRuteo es mayor a cero si se hizo la desfragmentación
                                                    etiquetaCantDesfrag.setText("" + (int) (Integer.parseInt("" + etiquetaCantDesfrag.getText()) + 1)); //suma 1
                                                }
                                                break;
                                        }
                                        if (desfragmentarFS) {
                                            Algoritmos_Defrag_ProAct.defragmentarFS(G[core][0], capacidadPorEnlace);
                                        }
                                    } catch (IOException ex) {
                                        Logger.getLogger(VentanaPrincipal_Defrag_ProAct.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            }
                            break;
                        }
                        case "DT Variable Paper": {
                            double fragIndex = 0, probBth;
                            deltatiempo++;
                            System.out.println("DeltaTiempo: " + deltatiempo);
                            probBth = cantBloqueosPaper / (deltatiempo * 1.0);
                            System.out.println("Prob Bloqueo: " + probBth);
                            fragIndex = Metricas.BFR(G[core][0], capacidadPorEnlace);
                            //fragIndex = Metricas.MSI(G[0],capacidadPorEnlace);
                            //fragIndex = G[0].entropia();
                            //System.out.println("BFR T VARIABLE: " + bfrGrafo);
                            //System.out.println("ultimo desfrag: "+ultimoDesfrag);
                            //if((i >= (ultimoDesfrag-20) && i <= (ultimoDesfrag+20))  && (fragIndex > this.probBloqueoBth ||  i == (ultimoDesfrag+20))  && i != tiempoTotal){// cada periodo y que no haga si es el ultimo tiempo
                            //con erlang 800 sin des: 251 con DT Vstisble:162 y 27 defrag, 186 fijo
                            // 600 erlang sin des: 46 dt variable 21 b 9 defrag - dtfijo 36 b 9 defrag
                            // 650 erlang sin des: 67 / dt variable 46 b 12 defrag - dtfijo 55
                            if ((ultDef + this.ventanaPar) <= i) {
                                if (probBth > this.probBloqueoBth && i != tiempoTotal) {// cada periodo y que no haga si es el ultimo tiempo
                                    //System.out.println("ultimo desfrag dentro: "+ultimoDesfrag);
                                    ultDef = i;
                                    cantBloqueosPaper = 0;
                                    deltatiempo = 0;
                                    ultimoDesfrag = ultimoDesfrag + periodoDesfrag;
                                    try {
                                        System.out.println("Inicia desfragmentacion en tiempo " + i + " con " + arrayRutas.size() + " rutas activas");
                                        switch (metodoDesfrag) {
                                            case "ACO":
                                                encontroSolucion = Utilitarios.desfragmentacionACO(topologia, RSA.get(0), resultadoRuteo.get(core), arrayRutas.get(core), mejoraACO, capacidadPorEnlace, G[core][0], listaKSP.get(core), archivoDefrag, i, cantHormACO, caminosDeDosEnlaces.get(core), this.jTableEstadoEnlaces, FSMinPC, objetivoACO, core);
                                                break;
                                            case "AG":
                                                encontroSolucionAG = Utilitarios.desfragmentacionAG(topologia, RSA.get(0), resultadoRuteo.get(core), arrayRutas.get(core), porcentajeLongCRAG, capacidadPorEnlace, G[core][0], listaKSP.get(core), archivoDefrag, i, cantIndividuosAG, objetivoAG, cantGeneracionesAG, core);
                                                break;
                                            default:
                                                resultadoReRuteo = Utilitarios.desfragmentacionPeoresRutas(topologia, G[core][0], capacidadPorEnlace, arrayRutas.get(core), resultadoRuteo.get(core), listaKSP.get(core), ObjetivoReruteo, porcRutasARerutear, FSMinPC, algoritmoAejecutar, rutasEstablecidas.get(core), core);
                                                //suma el resultado
                                                etiquetaCantRutasReruteadas.setText("" + (int) (Integer.parseInt("" + etiquetaCantRutasReruteadas.getText()) + resultadoReRuteo));
                                                if (resultadoReRuteo > 0) { //si resultadoReRuteo es mayor a cero si se hizo la desfragmentación
                                                    etiquetaCantDesfrag.setText("" + (int) (Integer.parseInt("" + etiquetaCantDesfrag.getText()) + 1)); //suma 1
                                                }
                                                break;
                                        }
                                        if (desfragmentarFS) {
                                            Algoritmos_Defrag_ProAct.defragmentarFS(G[core][0], capacidadPorEnlace);
                                        }
                                    } catch (IOException ex) {
                                        Logger.getLogger(VentanaPrincipal_Defrag_ProAct.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                            }
                            break;
                        }
                        default:
                            break;
                    }
                }

                //no modificar
                /*if(i==tiempoDesfrag){// || i==500 || i==700){
                    try {
                        Utilitarios.desfragmentacionACO(topologia,RSA.get(0), resultadoRuteo, arrayRutas, mejoraACO, capacidadPorEnlace, G[0], listaKSP, archivoDefrag, i, cantHormACO, caminosDeDosEnlaces, this.jTableEstadoEnlaces, FSMinPC,objetivoACO);
                    } catch (IOException ex) {
                        Logger.getLogger(VentanaPrincipal_Defrag_ProAct.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }*/
                //ESTE SI PODES MODIFICAR
//                if(haybloqueos){
//                    System.out.println("Path Consecutiveness Antes: "+ Metricas.PathConsecutiveness(caminosDeDosEnlaces, capacidadPorEnlace, G[0]));
//                    try {
//                        Utilitarios.seleccionDeRutasPathConsec(this.Redes.getTopologia(1),RSA.get(0), resultadoRuteo, arrayRutas, mejoraACO, capacidadPorEnlace, G[0], listaKSP, archivoDefrag, i, cantHormACO, this.jTableEstadoEnlaces, caminosDeDosEnlaces);
//                    } catch (IOException ex) {
//                        Logger.getLogger(VentanaPrincipal_Defrag_ProAct.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                                    System.out.println("Path Consecutiveness Despues: "+ Metricas.PathConsecutiveness(caminosDeDosEnlaces, capacidadPorEnlace, G[0]));
//                }
                contBloqueos = 0;

            }
            ++k;
            /*Inicio de impresion de  los datos de los estadisticos de bloqueos*/

            System.out.println("\nRanuras   Cantidad de Bloqueos\n");
            for (int aux = 1; aux < ranuras.length; aux++) {
                System.out.println("  " + aux + "\t\t" + ranuras[aux]);
            }
            System.out.println("\nLa  cantidad de bloqueos total : " + cantidadTotalBloqueos);
            System.out.println("la cantidad de desfragmentaciones es: ");

            /*Fin de la impresion de la estadistica de bloqueos*/
            // almacenamos la probablidad de bloqueo final para cada algoritmo
            for (int a = 0; a < RSA.size(); a++) {
                prob[a].add(((double) contB[a] / contD));
                System.out.println("Probabilidad: " + (double) prob[a].get(k) + " Algoritmo: " + RSA.get(a));
            }
            this.etiquetaError.setText("Simulacion Terminada...");

            //Si no existe escibir 0,0,0
            if (!archivoDefrag.exists()) {
                try {
                    try ( BufferedWriter bw = new BufferedWriter(new FileWriter(archivoDefrag))) {
                        bw.write("" + 0);
                        bw.write(",");
                        bw.write("" + 0);
                        bw.write(",");
                        bw.write("" + 0);
                        bw.write(",");
                        bw.write("" + 0);
                        bw.write(",");
                        bw.write("" + 0);
                        bw.write("\r\n");
                    }
                } catch (IOException ex) {
                    Logger.getLogger(VentanaPrincipal_Defrag_ProAct.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            //RESULTADOS

            //suma de tiempos de vida
            System.out.println("Suma de tiempos de vida: " + sumaTiempoDeVida);

            // una vez finalizado, graficamos el resultado.
            //leemos el archivo de resultados
            List<XYTextAnnotation> annotation = new LinkedList<>();
            String linea;
            int contLinea = 0;

            XYSeries series[][] = new XYSeries[this.cores][9];

            //tablas
            DefaultTableModel modelBloqueos = (DefaultTableModel) this.jTableResultadosBloqueos.getModel(); //todos
            DefaultTableModel modelResultados = (DefaultTableModel) this.jTableResultados.getModel(); //bloqueos

            //for(int core = 0; core<this.cores; core++) {
            int core = 0;
            FileReader fr;
            try {
                fr = new FileReader(archivoResultados);
                BufferedReader br = new BufferedReader(fr);
                series[core][0] = new XYSeries("Bloqueos");
                series[core][1] = new XYSeries("Entropía");
                series[core][2] = new XYSeries("ShanonEntropy");
                series[core][3] = new XYSeries("BFR");
                series[core][4] = new XYSeries("Cantidad de Light Paths");
                series[core][5] = new XYSeries("ABP");
                series[core][6] = new XYSeries("Entropía por su uso");
                series[core][7] = new XYSeries("% Uso");
                series[core][8] = new XYSeries("Prob Bloqueo");

                while (((linea = br.readLine()) != null)) {
                    contLinea++;
                    String[] line = linea.split(",", 12);
                    if (line[11].equals("" + core)) {

                        //agrega a la tabla los Resultados
                        modelResultados.addRow(new Object[]{line[0], line[1], line[2], (double) Double.parseDouble(line[3]), (double) Double.parseDouble(line[4]), (double) Double.parseDouble(line[5]), (double) Double.parseDouble(line[6]), (double) Double.parseDouble(line[7]), (double) Double.parseDouble(line[8]), (double) Double.parseDouble(line[9]), (double) Double.parseDouble(line[10])});

                        //agrega en annotation todos los bloqueos para después agregarlos a los gráficos
                        if ((double) Double.parseDouble(line[2]) > 0) {
                            annotation.add(new XYTextAnnotation(line[2], (double) Double.parseDouble(line[0]), 0.02));
                            //agrega a la tabla los bloqueos
                            modelBloqueos.addRow(new Object[]{line[0], line[1], line[2], (double) Double.parseDouble(line[3]), (double) Double.parseDouble(line[4]), (double) Double.parseDouble(line[5]), (double) Double.parseDouble(line[6]), (double) Double.parseDouble(line[7]), (double) Double.parseDouble(line[8]), (double) Double.parseDouble(line[9]), (double) Double.parseDouble(line[10])});
                        }

                        series[core][0].add(contLinea, (double) Double.parseDouble(line[2]));
                        series[core][1].add(contLinea, (double) Double.parseDouble(line[3]));
                        series[core][2].add(contLinea, (double) Double.parseDouble(line[4]));
                        series[core][3].add(contLinea, (double) Double.parseDouble(line[5]));
                        series[core][4].add(contLinea, (double) Double.parseDouble(line[6]));
                        series[core][5].add(contLinea, (double) Double.parseDouble(line[7]));
                        series[core][6].add(contLinea, (double) Double.parseDouble(line[8]));
                        series[core][7].add(contLinea, (double) Double.parseDouble(line[9]));
                        series[core][8].add(contLinea, (double) Double.parseDouble(line[10]));
                    }
                }

                //hallar el max y min de los resultados
                guardarMaxMin(this.jTableResultados, this.jTableResultadosMinMax);

                //hallar el max y min de los bloqueos
                if (contB[0] != 0) {
                    guardarMaxMin(this.jTableResultadosBloqueos, this.jTableResultadosBloqueosMinMax);
                }

                //graficar
                Utilitarios.GraficarResultado(series[core], annotation, this.panelResultados);

                //estado final de los enlaces
                //                Utilitarios.actualizarTablaEstadoEnlaces(G[0], jTableResultados, capacidadPorEnlace);
            } catch (IOException ioe) {
                Logger.getLogger(VentanaPrincipal_Defrag_ProAct.class.getName()).log(Level.SEVERE, null, ioe);
            }

            //tabla desfragmentaciones
            //solo si fue ACO porque peores rutas no guarda en el archivo, escribe directamente al terminar
            if (metodoDesfrag.equals("ACO")) {
                Integer[] resultDefrags = new Integer[2];
                String rutaResultadosDefrag = System.getProperty("user.dir") + "\\src\\Defrag\\ProAct\\Archivos\\Resultados\\Defrag" + detallesNombre;
                File archivoResultadosDefrag = new File(rutaResultadosDefrag);
                try {
                    resultDefrags = Utilitarios.cargarTablaResultadosDefrag(archivoResultadosDefrag, this.jTableResultadosDefrag);
                } catch (IOException ex) {
                    Logger.getLogger(VentanaPrincipal_Defrag_ProAct.class.getName()).log(Level.SEVERE, null, ex);
                }

                //imprime los resultados en la pantalla
                this.etiquetaCantDesfrag.setText("" + resultDefrags[0]);
                this.etiquetaCantRutasReruteadas.setText("" + resultDefrags[1]);
            }
            if (metodoDesfrag.equals("AG")) {
                Integer[] resultDefrags = new Integer[2];
                String rutaResultadosDefrag = System.getProperty("user.dir") + "\\src\\Defrag\\ProAct\\Archivos\\Resultados\\Defrag" + detallesNombre;
                File archivoResultadosDefrag = new File(rutaResultadosDefrag);
                try {
                    resultDefrags = Utilitarios.cargarTablaResultadosDefrag(archivoResultadosDefrag, this.jTableResultadosDefrag);
                } catch (IOException ex) {
                    Logger.getLogger(VentanaPrincipal_Defrag_ProAct.class.getName()).log(Level.SEVERE, null, ex);
                }

                //imprime los resultados en la pantalla
                this.etiquetaCantDesfrag.setText("" + resultDefrags[0]);
                this.etiquetaCantRutasReruteadas.setText("" + resultDefrags[1]);
            }

            //Utilitarios.GraficarResultado(prob, this.panelResultado, "Resultado de la Simulación", RSA, paso);
            String demandasTotales = "" + contD; // mostramos la cantidad de demandas totales recibidas
            this.etiquetaDemandasTotales.setText(demandasTotales);
            this.etiquetaBloqueosTotales.setText("" + contB[0]);
            this.etiquetaTextoBloqueosTotales.setVisible(true);
            this.etiquetaDemandasTotales.setVisible(true);
            this.etiquetaTextoDemandasTotales.setVisible(true);
            this.etiquetaBloqueosTotales.setVisible(true);
            this.etiquetaTextoCantDesfrag.setVisible(true);
            this.etiquetaCantDesfrag.setVisible(true);
            this.etiquetaTextoCantRutasReruteadas.setVisible(true);
            this.etiquetaCantRutasReruteadas.setVisible(true);

            ////////Vaciar listas para las siguientes simulaciones///////////////
            /////////////////////////////////////////////////////////////////////
            //this.algoritmosCompletosParaEjecutar.clear();
            //this.algoritmosCompletosParaGraficar.clear();
            //this.cantidadDeAlgoritmosRuteoSeleccionados = 0;
            this.cantidadDeAlgoritmosTotalSeleccionados = 0;

        } else { // control de errores posibles realizados al no completar los parametros de simulacion
            if (this.listaAlgoritmosRuteo.getSelectedIndex() < 0) {
                if (mensajeError.equals("Seleccione ")) {
                    mensajeError = mensajeError + "Algoritmo RSA";
                } else {
                    mensajeError = mensajeError + ", Algoritmo RSA";
                }
            }
            if (this.listaRedes.getSelectedIndex() < 0) {
                if (mensajeError.equals("Seleccione ")) {
                    mensajeError = mensajeError + "Topologia";
                } else {
                    mensajeError = mensajeError + ", Topologia";
                }
            }
            if (mensajeError.equals("Seleccione ")) {
                this.etiquetaError.setText(mensajeError);
            }
        }
    }//GEN-LAST:event_botonEjecutarSimulacionActionPerformed

    // get the maximum and the minimum
    public void guardarMaxMin(JTable Tabla, JTable TablaMaxMin) {
        DefaultTableModel model = (DefaultTableModel) TablaMaxMin.getModel();
        ArrayList<Double> list0 = new ArrayList<>();
        ArrayList<Double> list1 = new ArrayList<>();
        ArrayList<Double> list2 = new ArrayList<>();
        ArrayList<Double> list3 = new ArrayList<>();
        ArrayList<Double> list4 = new ArrayList<>();
        ArrayList<Double> list5 = new ArrayList<>();
        ArrayList<Double> list6 = new ArrayList<>();
        ArrayList<Double> list7 = new ArrayList<>();
        for (int i = 0; i < Tabla.getRowCount(); i++) {
            list0.add(Double.parseDouble(Tabla.getValueAt(i, 3).toString()));
            list1.add(Double.parseDouble(Tabla.getValueAt(i, 4).toString()));
            list2.add(Double.parseDouble(Tabla.getValueAt(i, 5).toString()));
            list3.add(Double.parseDouble(Tabla.getValueAt(i, 6).toString()));
            list4.add(Double.parseDouble(Tabla.getValueAt(i, 7).toString()));
            list5.add(Double.parseDouble(Tabla.getValueAt(i, 8).toString()));
            list6.add(Double.parseDouble(Tabla.getValueAt(i, 9).toString()));
            list7.add(Double.parseDouble(Tabla.getValueAt(i, 10).toString()));
        }

        Double maxEntro;
        Double minEntro;
        Double maxMSI;
        Double minMSI;
        Double maxBRF;
        Double minBRF;
        Double maxLP;
        Double minLP;
        Double maxPC;
        Double minPC;
        Double maxEntroUso;
        Double minEntroUso;
        Double maxPorcUso;
        Double minPorcUso;
        Double maxPorcBloqueo;
        Double minPorcBloqueo;

        maxEntro = Collections.max(list0);
        minEntro = Collections.min(list0);
        maxMSI = Collections.max(list1);
        minMSI = Collections.min(list1);
        maxBRF = Collections.max(list2);
        minBRF = Collections.min(list2);
        maxLP = Collections.max(list3);
        minLP = Collections.min(list3);
        maxPC = Collections.max(list4);
        minPC = Collections.min(list4);
        maxEntroUso = Collections.max(list5);
        minEntroUso = Collections.min(list5);
        maxPorcUso = Collections.max(list6);
        minPorcUso = Collections.min(list6);
        maxPorcBloqueo = Collections.max(list7);
        minPorcBloqueo = Collections.min(list7);

        //agrega a la tabla los bloqueos
        model.addRow(new Object[]{minEntro, minMSI, minBRF, minLP, minPC, minEntroUso, minPorcUso, minPorcBloqueo});
        model.addRow(new Object[]{maxEntro, maxMSI, maxBRF, maxLP, maxPC, maxEntroUso, maxPorcUso, maxPorcBloqueo});
//        Tmax.setText(Integer.toString(max));
//        Tmin.setText(Integer.toString(min));
    }


    private void listaAlgoritmosRuteoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listaAlgoritmosRuteoMouseClicked
        // TODO add your handling code here:
//        List algoritmosRuteoSeleccionados = this.listaAlgoritmosRuteo.getSelectedValuesList();
//        String algoritmoSeleccionado = (String) algoritmosRuteoSeleccionados.get(0);
//        //System.out.println("El algoritmosRuteoSeleccionados22:"+algoritmoSeleccionado);
//        if (algoritmoSeleccionado.equals("FAR")) {
//            this.panelAsignacionSpectro.setVisible(true);
//        } else {
//            this.panelAsignacionSpectro.setVisible(false);
//        }


    }//GEN-LAST:event_listaAlgoritmosRuteoMouseClicked

    private void listaRedesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listaRedesActionPerformed
        setearRed();
    }//GEN-LAST:event_listaRedesActionPerformed

    private void setearRed() {
        if (this.listaRedes.getSelectedIndex() >= 0) {
            ImageIcon Img = new ImageIcon();

            String redseleccionada = (String) this.listaRedes.getSelectedItem();
            switch (redseleccionada) {
                case "NSFNet":
                    Img = new ImageIcon(getClass().getResource(("Imagenes/" + ("Red 1.png"))));
                    //this.textFieldCapacidadEnlace.setText(Integer.toString((int) (this.Redes.getRed(1).getCapacidadTotal() / this.Redes.getRed(1).getAnchoFS())));
                    this.textFieldCapacidadEnlace.setText("320");
                    this.textFieldAnchoFS.setText(Double.toString(this.Redes.getRed(1).getAnchoFS()));
                    break;
                case "ARPA-2":
                    Img = new ImageIcon(getClass().getResource(("Imagenes/" + ("Red 2.png"))));
                    //this.textFieldCapacidadEnlace.setText(Integer.toString((int) (this.Redes.getRed(2).getCapacidadTotal() / this.Redes.getRed(2).getAnchoFS())));
                    this.textFieldCapacidadEnlace.setText("320");
                    this.textFieldAnchoFS.setText(Double.toString(this.Redes.getRed(2).getAnchoFS()));
                    break;
                case "USNet":
                    Img = new ImageIcon(getClass().getResource(("Imagenes/" + ("Red 6.png"))));
                    //this.textFieldCapacidadEnlace.setText(Integer.toString((int) (this.Redes.getRed(3).getCapacidadTotal() / this.Redes.getRed(3).getAnchoFS())));
                    this.textFieldCapacidadEnlace.setText("320");
                    this.textFieldAnchoFS.setText(Double.toString(this.Redes.getRed(3).getAnchoFS()));
                    break;
            }

            etiquetaImagenTopologia.setBounds(150, 110, 150, 110);
            etiquetaImagenTopologia.setIcon(Img);
            etiquetaImagenTopologia.setVisible(true);
            etiquetaImagenTopologia.setOpaque(false);
        }
    }


    private void textFieldLambdaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldLambdaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldLambdaActionPerformed

    private void textFieldAnchoFSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldAnchoFSActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldAnchoFSActionPerformed

    private void textFieldFSmaximoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldFSmaximoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldFSmaximoActionPerformed

    private void ComboMetodoDesfragActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboMetodoDesfragActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ComboMetodoDesfragActionPerformed

    private void ComboMetodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboMetodoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ComboMetodoActionPerformed

    private void textFieldFSminimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldFSminimoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldFSminimoActionPerformed

    private void textFieldPeriodoDesfragActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldPeriodoDesfragActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldPeriodoDesfragActionPerformed

    private void textFieldMejoraACOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldMejoraACOActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldMejoraACOActionPerformed

    private void textFieldCantHormigasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldCantHormigasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldCantHormigasActionPerformed

    private void ComboObjetivoACOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboObjetivoACOActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ComboObjetivoACOActionPerformed

    private void ComboObjetivoReruteoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboObjetivoReruteoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ComboObjetivoReruteoActionPerformed

    private void textFieldRutasARerutearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldRutasARerutearActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldRutasARerutearActionPerformed

    private void jTextFieldCantIndividuosAGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCantIndividuosAGActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldCantIndividuosAGActionPerformed

    private void jTextFieldMejoraAGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldMejoraAGActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldMejoraAGActionPerformed

    private void jTextFieldCantGeneracionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCantGeneracionesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldCantGeneracionesActionPerformed

    private void textFieldCoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldCoresActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldCoresActionPerformed

    private void textFieldCapacidadEnlaceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textFieldCapacidadEnlaceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textFieldCapacidadEnlaceActionPerformed

    private void ComboOrdenarDemandasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboOrdenarDemandasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ComboOrdenarDemandasActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            javax.swing.UIManager.setLookAndFeel(new FlatDarkLaf());
        } catch (UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaPrincipal_Defrag_ProAct.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new VentanaPrincipal_Defrag_ProAct().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> ComboMetodo;
    private javax.swing.JComboBox<String> ComboMetodoDesfrag;
    private javax.swing.JComboBox ComboMetrica;
    private javax.swing.JComboBox<String> ComboObjAlgoritmoGenetico;
    private javax.swing.JComboBox<String> ComboObjetivoACO;
    private javax.swing.JComboBox<String> ComboObjetivoReruteo;
    private javax.swing.JComboBox<String> ComboOrdenarDemandas;
    private javax.swing.JComboBox<String> ComboTipoTrafico;
    private javax.swing.JButton botonEjecutarSimulacion;
    private javax.swing.JCheckBox checkDesfragFS;
    private javax.swing.JLabel etiquetaAnchoFSActual;
    private javax.swing.JLabel etiquetaAnchoFSActual1;
    private javax.swing.JLabel etiquetaAnchoFSActual12;
    private javax.swing.JLabel etiquetaAnchoFSActual19;
    private javax.swing.JLabel etiquetaAnchoFSActual2;
    private javax.swing.JLabel etiquetaAnchoFSActual20;
    private javax.swing.JLabel etiquetaAnchoFSActual22;
    private javax.swing.JLabel etiquetaAnchoFSActual23;
    private javax.swing.JLabel etiquetaAnchoFSActual26;
    private javax.swing.JLabel etiquetaAnchoFSActual3;
    private javax.swing.JLabel etiquetaAnchoFSActual4;
    private javax.swing.JLabel etiquetaAnchoFSActual6;
    private javax.swing.JLabel etiquetaAnchoFSActual7;
    private javax.swing.JLabel etiquetaAnchoFSActual8;
    private javax.swing.JLabel etiquetaBloqueosTotales;
    private javax.swing.JLabel etiquetaCantDesfrag;
    private javax.swing.JLabel etiquetaCantRutasReruteadas;
    private javax.swing.JLabel etiquetaCapacidadActual;
    private javax.swing.JLabel etiquetaDemandasTotales;
    private javax.swing.JLabel etiquetaError;
    private javax.swing.JLabel etiquetaImagenTopologia;
    private javax.swing.JLabel etiquetaProbBloqueo;
    private javax.swing.JLabel etiquetaRSA1;
    private javax.swing.JLabel etiquetaRSA2;
    private javax.swing.JLabel etiquetaRSA3;
    private javax.swing.JLabel etiquetaTextoBloqueosTotales;
    private javax.swing.JLabel etiquetaTextoCantDesfrag;
    private javax.swing.JLabel etiquetaTextoCantRutasReruteadas;
    private javax.swing.JLabel etiquetaTextoDemandasTotales;
    private javax.swing.JLabel etiquetaTextoMax;
    private javax.swing.JLabel etiquetaTextoMax1;
    private javax.swing.JLabel etiquetaTextoMin;
    private javax.swing.JLabel etiquetaTextoMin1;
    private javax.swing.JLabel etiquetaTiempoActual;
    private javax.swing.JLabel etiquetaTopologia;
    private javax.swing.JLabel etiquetaTopologia1;
    private javax.swing.JLabel etiquetaTopologia2;
    private javax.swing.JLabel etiquetaVentana;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTable jTableEstadoEnlaces;
    private javax.swing.JTable jTableResultados;
    private javax.swing.JTable jTableResultadosBloqueos;
    private javax.swing.JTable jTableResultadosBloqueosMinMax;
    private javax.swing.JTable jTableResultadosDefrag;
    private javax.swing.JTable jTableResultadosMinMax;
    private javax.swing.JTextField jTextFieldCantGeneraciones;
    private javax.swing.JTextField jTextFieldCantIndividuosAG;
    private javax.swing.JTextField jTextFieldMejoraAG;
    private javax.swing.JTextField jTextFieldProbBloqueo;
    private javax.swing.JTextField jTextFieldVentana;
    private java.awt.Label label1;
    private javax.swing.JList<String> listaAlgoritmosRuteo;
    private javax.swing.JComboBox<String> listaRedes;
    private javax.swing.JScrollPane panelResultados;
    private javax.swing.JSpinner spinnerErlang;
    private javax.swing.JSpinner spinnerTiempoSimulacion;
    private javax.swing.JTextField textFieldAnchoFS;
    private javax.swing.JTextField textFieldCantHormigas;
    private javax.swing.JTextField textFieldCapacidadEnlace;
    private javax.swing.JTextField textFieldCores;
    private javax.swing.JTextField textFieldFSmaximo;
    private javax.swing.JTextField textFieldFSminimo;
    private javax.swing.JTextField textFieldLambda;
    private javax.swing.JTextField textFieldMejoraACO;
    private javax.swing.JTextField textFieldPeriodoDesfrag;
    private javax.swing.JTextField textFieldRutasARerutear;
    // End of variables declaration//GEN-END:variables

}
