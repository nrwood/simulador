/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EON.Algoritmos;

import EON.Demanda;
import EON.Enlace;
import EON.FrecuencySlots;
import EON.GrafoMatriz;
import EON.ListaEnlazada;
import EON.ListaEnlazadaAsignadas;
import EON.Nodo;
import EON.Resultado;
import EON.Utilitarios.Utilitarios;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


/**
 *
 * @author sFernandez
 */
public class Algoritmos_Defrag_ProAct {

    
    public static Resultado Def_FACA(GrafoMatriz G, Demanda demanda,ListaEnlazada [] ksp,int capacidad){
        
        //*Definicion de variables las variables
        int cont; // posicion inicial y final dentro del espectro asi como el contador de FSs contiguos disponibles
        int sgteBloque;//bandera para avisar que tiene que ir al siguiente bloque
        
//        int demandaColocada=0; // bandera para controlar si ya se encontro espectro disponible para la demanda.
        int [] OE= new int[capacidad]; //Ocupacion de Espectro.
        ArrayList<ListaEnlazada> kspUbicados = new ArrayList<>();
        ArrayList<Integer> inicios = new ArrayList<>();
        ArrayList<Integer> fines = new ArrayList<>();
        ArrayList<Integer> indiceKsp = new ArrayList<>();
        ArrayList<Integer> indKSPUbicMenFcmt = new ArrayList<>();

        //Probamos para cada camino, si existe espectro para ubicar la damanda
        int k=0;

        while(k<ksp.length && ksp[k]!=null){
            //Inicializadomos el espectro, inicialmente todos los FSs estan libres
            for(int i=0;i<capacidad;i++){
                OE[i]=1;
            }
            //Calcular la ocupacion del espectro para cada camino k
            for(int i=0;i<capacidad;i++){
                for(Nodo n=ksp[k].getInicio();n.getSiguiente().getSiguiente()!=null;n=n.getSiguiente()){
                   //System.out.println("v1 "+n.getDato()+" v2 "+n.getSiguiente().getDato()+" cant vertices "+G.getCantidadDeVertices()+" i "+i+" FSs "+G.acceder(n.getDato(),n.getSiguiente().getDato()).getFS().length);
                    if(G.acceder(n.getDato(),n.getSiguiente().getDato()).getFS()[i].getEstado()==0){
                        OE[i]=0;
                        break;
                    }
                }
            }
            
            //encuentra las posibles asignaciones
            cont=0;
            sgteBloque = 0; 
            for(int i=0;i<capacidad;i++){
                if(OE[i]==1 && sgteBloque == 0){
                    cont++;
                }else if (OE[i]==0){
                    cont=0;
                    sgteBloque = 0;
                }
                //si se encontro un bloque valido, tomamos en cuenta el ksp
                if(cont==demanda.getNroFS()){
                    fines.add(i);
                    inicios.add(i - cont + 1);
                    kspUbicados.add(ksp[k]);
                    indiceKsp.add(k);
                    sgteBloque = 1;
                    cont = 0;
//                    break; //solo agrega el primero que encuentra
                }
            }
            k++;
        }
        
        if (kspUbicados.isEmpty()){ //bloqueo
            //System.out.println("Desubicado");
            return null;
        }
        
        //cuenta los cortes para cada posible asiganción
//        int cutsSlot; //cantidad de cortes
        int ind = 0; //aux indice del kspUbicado actual
        int corte = 999; //cant de cortes de la opcion del ksp
        Resultado r = new Resultado();
        double FcmtAux = -1;
        double Fcmt = 9999999;
        int caminoElegido = -1;
        
        //por cada índice de los posibles caminos de cada KSP ubicado
        for (ListaEnlazada kspUbi : kspUbicados){
            corte = 0;
            for (Nodo n = kspUbi.getInicio(); n.getSiguiente().getSiguiente() != null; n = n.getSiguiente()) {
                if (inicios.get(ind) != 0 && fines.get(ind) < capacidad - 1) { //para que no tome los bordes sup e inf
                    if (G.acceder(n.getDato(), n.getSiguiente().getDato()).getFS()[inicios.get(ind) - 1].getEstado() == 1
                            && G.acceder(n.getDato(), n.getSiguiente().getDato()).getFS()[fines.get(ind) + 1].getEstado() == 1) {
                        corte = corte + 1;
                    }
                }
            }
            
            //calcula el desalineamiento de cada uno
            int Desalineacion = 0;
            int bandEsRuta = 0;
            for (Nodo n = kspUbi.getInicio(); n.getSiguiente().getSiguiente() != null; n = n.getSiguiente()) {
                for (int i = 0; i < G.getCantidadDeVertices(); i++) {
                    if(G.acceder(n.getDato(), i)!=null){ //si son vecinos
                        bandEsRuta = 0;
                        for (Nodo nn = kspUbi.getInicio(); nn.getSiguiente().getSiguiente() != null; nn = nn.getSiguiente()) {
                            if(nn.getDato() == i){ //si es parte de la ruta
                                bandEsRuta = 1;
                            }
                        }
                        if(bandEsRuta == 0){
                            for(int fsd = inicios.get(ind); fsd <= inicios.get(ind) + demanda.getNroFS() - 1; fsd++){
                                if (G.acceder(i, n.getDato()).getFS()[fsd].getEstado() == 1) {
                                    Desalineacion = Desalineacion + 1;
                                } else {
                                    Desalineacion = Desalineacion - 1;
                                }
                            }
                        }
                    }
                }
            }
            
            //calcular la capacidad actual
            double capacidadLibre = (double)Utilitarios.contarCapacidadLibre(kspUbi,G,capacidad);
        
            double saltos = (double)Utilitarios.calcularSaltos(kspUbi);
            double vecinos = (double)Utilitarios.contarVecinos(kspUbi,G,capacidad);
            
            FcmtAux = corte + (Desalineacion/(demanda.getNroFS()*vecinos)) + (saltos *(demanda.getNroFS()/capacidadLibre)); 
            
            if (FcmtAux<Fcmt){
                Fcmt = FcmtAux;
                indKSPUbicMenFcmt.clear();
                indKSPUbicMenFcmt.add(ind);
            }else if(FcmtAux==Fcmt){
                indKSPUbicMenFcmt.add(ind);
            }
        
            ind++;
        }
        
        if (indKSPUbicMenFcmt.size() == 1){
            r.setCamino(indiceKsp.get(indKSPUbicMenFcmt.get(0)));
            r.setFin(fines.get(indKSPUbicMenFcmt.get(0)));
            r.setInicio(inicios.get(indKSPUbicMenFcmt.get(0)));
        }else{
            //encontrar el ksp más corto
            int tamKspMasCorto = 999;
            int indKspMasCorto = -1;
            for (int indMenCutsMenDesalig : indKSPUbicMenFcmt) {
                if (kspUbicados.get(indMenCutsMenDesalig).getTamanho() - 1 < tamKspMasCorto){
                    indKspMasCorto = indMenCutsMenDesalig;
                    tamKspMasCorto = kspUbicados.get(indMenCutsMenDesalig).getTamanho() - 1;
                }
            }
            //buscar el indice first fit
            //Inicializadomos el espectro, inicialmente todos los FSs estan libres
            for(int i=0;i<capacidad;i++){
                OE[i]=1;
            }
            //Calcular la ocupacion del espectro para cada camino k
            for(int i=0;i<capacidad;i++){
                for(Nodo n=ksp[indiceKsp.get(indKspMasCorto)].getInicio();n.getSiguiente().getSiguiente()!=null;n=n.getSiguiente()){
                    if(G.acceder(n.getDato(),n.getSiguiente().getDato()).getFS()[i].getEstado()==0){
                        OE[i]=0;
                        break;
                    }
                }
            }
            cont=0; 
            for(int i=0;i<capacidad;i++){
                if(OE[i]==1){
                    cont++;
                }else if (OE[i]==0){
                    cont=0;
                }
                //si se encontro un bloque valido, tomamos en cuenta el ksp
                if(cont==demanda.getNroFS()){ //si o si va a encontrar ya que es un ksp en kspUbicados
                    r.setCamino(indiceKsp.get(indKspMasCorto));
                    r.setFin(i);
                    r.setInicio(i - cont + 1);
                    break; //solo el primero que encuentra
                }
            }
        }
        

        return r;
    }

    
    public static Resultado Def_FACA(GrafoMatriz[][] G, Demanda demanda, ListaEnlazada[] ksp,int capacidad, int a){
        
        //*Definicion de variables las variables
        int cont; // posicion inicial y final dentro del espectro asi como el contador de FSs contiguos disponibles
        int sgteBloque;//bandera para avisar que tiene que ir al siguiente bloque
        
//        int demandaColocada=0; // bandera para controlar si ya se encontro espectro disponible para la demanda.
        int [] OE= new int[capacidad]; //Ocupacion de Espectro.
        ArrayList<ListaEnlazada> kspUbicados = new ArrayList<>();
        ArrayList<Integer> inicios = new ArrayList<>();
        ArrayList<Integer> fines = new ArrayList<>();
        ArrayList<Integer> indiceKsp = new ArrayList<>();
        ArrayList<Integer> indKSPUbicMenFcmt = new ArrayList<>();

        //Probamos para cada camino, si existe espectro para ubicar la damanda
        int k=0;

        while(k<ksp.length && ksp[k]!=null){
            //Inicializadomos el espectro, inicialmente todos los FSs estan libres
            for(int i=0;i<capacidad;i++){
                OE[i]=1;
            }
            //Calcular la ocupacion del espectro para cada camino k
            for(int i=0;i<capacidad;i++){
                for(Nodo n=ksp[k].getInicio();n.getSiguiente().getSiguiente()!=null;n=n.getSiguiente()){
                   //System.out.println("v1 "+n.getDato()+" v2 "+n.getSiguiente().getDato()+" cant vertices "+G.getCantidadDeVertices()+" i "+i+" FSs "+G.acceder(n.getDato(),n.getSiguiente().getDato()).getFS().length);
                    if(G[ksp[k].getCore()][a].acceder(n.getDato(),n.getSiguiente().getDato()).getFS()[i].getEstado()==0){
                        OE[i]=0;
                        break;
                    }
                }
            }
            
            //encuentra las posibles asignaciones
            cont=0;
            sgteBloque = 0; 
            for(int i=0;i<capacidad;i++){
                if(OE[i]==1 && sgteBloque == 0){
                    cont++;
                }else if (OE[i]==0){
                    cont=0;
                    sgteBloque = 0;
                }
                //si se encontro un bloque valido, tomamos en cuenta el ksp
                if(cont==demanda.getNroFS()){
                    fines.add(i);
                    inicios.add(i - cont + 1);
                    kspUbicados.add(ksp[k]);
                    indiceKsp.add(k);
                    sgteBloque = 1;
                    cont = 0;
//                    break; //solo agrega el primero que encuentra
                }
            }
            k++;
        }
        
        if (kspUbicados.isEmpty()){ //bloqueo
            //System.out.println("Desubicado");
            return null;
        }
        
        //cuenta los cortes para cada posible asiganción
//        int cutsSlot; //cantidad de cortes
        int ind = 0; //aux indice del kspUbicado actual
        int corte = 999; //cant de cortes de la opcion del ksp
        Resultado r = new Resultado();
        double FcmtAux = -1;
        double Fcmt = 9999999;
        int caminoElegido = -1;
        
        //por cada índice de los posibles caminos de cada KSP ubicado
        for (ListaEnlazada kspUbi : kspUbicados){
            corte = 0;
            for (Nodo n = kspUbi.getInicio(); n.getSiguiente().getSiguiente() != null; n = n.getSiguiente()) {
                if (inicios.get(ind) != 0 && fines.get(ind) < capacidad - 1) { //para que no tome los bordes sup e inf
                    if (G[kspUbi.getCore()][a].acceder(n.getDato(), n.getSiguiente().getDato()).getFS()[inicios.get(ind) - 1].getEstado() == 1
                            && G[kspUbi.getCore()][a].acceder(n.getDato(), n.getSiguiente().getDato()).getFS()[fines.get(ind) + 1].getEstado() == 1) {
                        corte = corte + 1;
                    }
                }
            }
            
            //calcula el desalineamiento de cada uno
            int Desalineacion = 0;
            int bandEsRuta = 0;
            for (Nodo n = kspUbi.getInicio(); n.getSiguiente().getSiguiente() != null; n = n.getSiguiente()) {
                for (int i = 0; i < G[kspUbi.getCore()][a].getCantidadDeVertices(); i++) {
                    if(G[kspUbi.getCore()][a].acceder(n.getDato(), i)!=null){ //si son vecinos
                        bandEsRuta = 0;
                        for (Nodo nn = kspUbi.getInicio(); nn.getSiguiente().getSiguiente() != null; nn = nn.getSiguiente()) {
                            if(nn.getDato() == i){ //si es parte de la ruta
                                bandEsRuta = 1;
                            }
                        }
                        if(bandEsRuta == 0){
                            for(int fsd = inicios.get(ind); fsd <= inicios.get(ind) + demanda.getNroFS() - 1; fsd++){
                                if (G[kspUbi.getCore()][a].acceder(i, n.getDato()).getFS()[fsd].getEstado() == 1) {
                                    Desalineacion = Desalineacion + 1;
                                } else {
                                    Desalineacion = Desalineacion - 1;
                                }
                            }
                        }
                    }
                }
            }
            
            //calcular la capacidad actual
            double capacidadLibre = (double)Utilitarios.contarCapacidadLibre(kspUbi,G[kspUbi.getCore()][a],capacidad);
        
            double saltos = (double)Utilitarios.calcularSaltos(kspUbi);
            double vecinos = (double)Utilitarios.contarVecinos(kspUbi,G[kspUbi.getCore()][a],capacidad);
            
            FcmtAux = corte + (Desalineacion/(demanda.getNroFS()*vecinos)) + (saltos *(demanda.getNroFS()/capacidadLibre)); 
            
            if (FcmtAux<Fcmt){
                Fcmt = FcmtAux;
                indKSPUbicMenFcmt.clear();
                indKSPUbicMenFcmt.add(ind);
            }else if(FcmtAux==Fcmt){
                indKSPUbicMenFcmt.add(ind);
            }
        
            ind++;
        }
        
        if (indKSPUbicMenFcmt.size() == 1){
            r.setCamino(indiceKsp.get(indKSPUbicMenFcmt.get(0)));
            r.setFin(fines.get(indKSPUbicMenFcmt.get(0)));
            r.setInicio(inicios.get(indKSPUbicMenFcmt.get(0)));
        }else{
            //encontrar el ksp más corto
            int tamKspMasCorto = 999;
            int indKspMasCorto = -1;
            for (int indMenCutsMenDesalig : indKSPUbicMenFcmt) {
                if (kspUbicados.get(indMenCutsMenDesalig).getTamanho() - 1 < tamKspMasCorto){
                    indKspMasCorto = indMenCutsMenDesalig;
                    tamKspMasCorto = kspUbicados.get(indMenCutsMenDesalig).getTamanho() - 1;
                }
            }
            //buscar el indice first fit
            //Inicializadomos el espectro, inicialmente todos los FSs estan libres
            for(int i=0;i<capacidad;i++){
                OE[i]=1;
            }
            //Calcular la ocupacion del espectro para cada camino k
            for(int i=0;i<capacidad;i++){
                for(Nodo n=ksp[indiceKsp.get(indKspMasCorto)].getInicio();n.getSiguiente().getSiguiente()!=null;n=n.getSiguiente()){
                    if(G[ksp[indiceKsp.get(indKspMasCorto)].getCore()][a].acceder(n.getDato(),n.getSiguiente().getDato()).getFS()[i].getEstado()==0){
                        OE[i]=0;
                        break;
                    }
                }
            }
            cont=0; 
            for(int i=0;i<capacidad;i++){
                if(OE[i]==1){
                    cont++;
                }else if (OE[i]==0){
                    cont=0;
                }
                //si se encontro un bloque valido, tomamos en cuenta el ksp
                if(cont==demanda.getNroFS()){ //si o si va a encontrar ya que es un ksp en kspUbicados
                    r.setCamino(indiceKsp.get(indKspMasCorto));
                    r.setFin(i);
                    r.setInicio(i - cont + 1);
                    break; //solo el primero que encuentra
                }
            }
        }
        

        return r;
    }
    
    public static Resultado Def_FA(GrafoMatriz G, Demanda demanda,ListaEnlazada [] ksp,int capacidad){
        
        //*Definicion de variables las variables
        int cont; // posicion inicial y final dentro del espectro asi como el contador de FSs contiguos disponibles
        int sgteBloque;//bandera para avisar que tiene que ir al siguiente bloque
        
//        int demandaColocada=0; // bandera para controlar si ya se encontro espectro disponible para la demanda.
        int [] OE= new int[capacidad]; //Ocupacion de Espectro.
        ArrayList<ListaEnlazada> kspUbicados = new ArrayList<>();
        ArrayList<Integer> inicios = new ArrayList<>();
        ArrayList<Integer> fines = new ArrayList<>();
        ArrayList<Integer> indiceKsp = new ArrayList<>();

        //Probamos para cada camino, si existe espectro para ubicar la damanda
        int k=0;

        while(k<ksp.length && ksp[k]!=null){
            //Inicializadomos el espectro, inicialmente todos los FSs estan libres
            for(int i=0;i<capacidad;i++){
                OE[i]=1;
            }
            //Calcular la ocupacion del espectro para cada camino k
            for(int i=0;i<capacidad;i++){
                for(Nodo n=ksp[k].getInicio();n.getSiguiente().getSiguiente()!=null;n=n.getSiguiente()){
                   //System.out.println("v1 "+n.getDato()+" v2 "+n.getSiguiente().getDato()+" cant vertices "+G.getCantidadDeVertices()+" i "+i+" FSs "+G.acceder(n.getDato(),n.getSiguiente().getDato()).getFS().length);
                    if(G.acceder(n.getDato(),n.getSiguiente().getDato()).getFS()[i].getEstado()==0){
                        OE[i]=0;
                        break;
                    }
                }
            }
            
            //encuentra las posibles asignaciones
            cont=0;
            sgteBloque = 0; 
            for(int i=0;i<capacidad;i++){
                if(OE[i]==1 && sgteBloque == 0){
                    cont++;
                }else if (OE[i]==0){
                    cont=0;
                    sgteBloque = 0;
                }
                //si se encontro un bloque valido, tomamos en cuenta el ksp
                if(cont==demanda.getNroFS()){
                    fines.add(i);
                    inicios.add(i - cont + 1);
                    kspUbicados.add(ksp[k]);
                    indiceKsp.add(k);
                    sgteBloque = 1;
                    cont = 0;
//                    break; //solo agrega el primero que encuentra
                }
            }
            k++;
        }
        
        if (kspUbicados.isEmpty()){ //bloqueo
            //System.out.println("Desubicado");
            return null;
        }
        
        //cuenta los cortes para cada posible asiganción
//        int cutsSlot; //cantidad de cortes
        int ind = 0; //aux indice del kspUbicado actual
        int cutAux = 0; //cant de cortes del camino ksp
        int cuts = 999; //el menor corte, 999 como referencia inicial
        Resultado r = new Resultado();
        int DesalineacionFinal = 999;
        
        //vectores con los menores cortes
        ArrayList<Integer> indKSPUbicMenCuts = new ArrayList<>();
        ArrayList<Integer> indKSPUbicMenCutsMenDesalig = new ArrayList<>();
        
        //por cada índice de los posibles caminos de cada KSP ubicado
        for (ListaEnlazada kspUbi : kspUbicados){
            for (Nodo n = kspUbi.getInicio(); n.getSiguiente().getSiguiente() != null; n = n.getSiguiente()) {
                if (inicios.get(ind) != 0 && fines.get(ind) < capacidad - 1) { //para que no tome los bordes sup e inf
                    if (G.acceder(n.getDato(), n.getSiguiente().getDato()).getFS()[inicios.get(ind) - 1].getEstado() == 1
                            && G.acceder(n.getDato(), n.getSiguiente().getDato()).getFS()[fines.get(ind) + 1].getEstado() == 1) {
                        cutAux = cutAux + 1;
                    }
                }
            }
            //encuentra el/los menor/es
            if (cutAux < cuts) {
                //si hay un menor al menor, limpia el vector y solo deja ese
                cuts = cutAux;
                indKSPUbicMenCuts.clear();
                indKSPUbicMenCuts.add(ind);
            }else if(cutAux == cuts){
                indKSPUbicMenCuts.add(ind);
            }
            cutAux = 0;
            ind++;
        }
        
        //si hay un solo menor cut entonces es elegido, sino se calcula el alineamiento de los menores
        if (indKSPUbicMenCuts.size() == 1){
            r.setCamino(indiceKsp.get(indKSPUbicMenCuts.get(0)));
            r.setFin(fines.get(indKSPUbicMenCuts.get(0)));
            r.setInicio(inicios.get(indKSPUbicMenCuts.get(0)));
        }else {
            //calcula el desalineamiento de cada uno
            for (int indMenorCuts : indKSPUbicMenCuts) {                
                //calcula el desalineamiento de cada uno
                int Desalineacion = 0;
                int bandEsRuta = 0;
                for (Nodo n = kspUbicados.get(indMenorCuts).getInicio(); n.getSiguiente().getSiguiente() != null; n = n.getSiguiente()) {
                    for (int i = 0; i < G.getCantidadDeVertices(); i++) {
                        if(G.acceder(n.getDato(), i)!=null){ //si son vecinos
                            bandEsRuta = 0;
                            for (Nodo nn = kspUbicados.get(indMenorCuts).getInicio(); nn.getSiguiente().getSiguiente() != null; nn = nn.getSiguiente()) {
                                if(nn.getDato() == i){ //si es parte de la ruta
                                    bandEsRuta = 1;
                                    break;
                                }
                            }
                            if(bandEsRuta == 0){
                                for(int fsd = inicios.get(indMenorCuts); fsd <= inicios.get(indMenorCuts) + demanda.getNroFS() - 1; fsd++){
                                    if (G.acceder(i, n.getDato()).getFS()[fsd].getEstado() == 1) {
                                        Desalineacion = Desalineacion + 1;
                                    } else {
                                        Desalineacion = Desalineacion - 1;
                                    }
                                }
                            }
                        }
                    }
                }

                if (Desalineacion < DesalineacionFinal) {
                    //si hay un menor al menor, limpia el vector y solo deja ese
                    DesalineacionFinal = Desalineacion;
                    indKSPUbicMenCutsMenDesalig.clear();
                    indKSPUbicMenCutsMenDesalig.add(indMenorCuts);
                }else if(Desalineacion == DesalineacionFinal){
                    indKSPUbicMenCutsMenDesalig.add(indMenorCuts);
                }
            }
            
            //si hay un solo menor cut con menor desalineación entonces es elegido, sino envie al shorter KSP y hace first Fit
            if (indKSPUbicMenCutsMenDesalig.size() == 1){
                r.setCamino(indiceKsp.get(indKSPUbicMenCutsMenDesalig.get(0)));
                r.setFin(fines.get(indKSPUbicMenCutsMenDesalig.get(0)));
                r.setInicio(inicios.get(indKSPUbicMenCutsMenDesalig.get(0)));
            }else { //calcula el shorter KSP y hace first Fit  
                //encontrar el ksp más corto
                int tamKspMasCorto = 999;
                int indKspMasCorto = -1;
                for (int indMenCutsMenDesalig : indKSPUbicMenCutsMenDesalig) {
                    if (kspUbicados.get(indMenCutsMenDesalig).getTamanho() - 1 < tamKspMasCorto){
                        indKspMasCorto = indMenCutsMenDesalig;
                        tamKspMasCorto = kspUbicados.get(indMenCutsMenDesalig).getTamanho() - 1;
                    }
                }
                //buscar el indice first fit
                //Inicializadomos el espectro, inicialmente todos los FSs estan libres
                for(int i=0;i<capacidad;i++){
                    OE[i]=1;
                }
                //Calcular la ocupacion del espectro para cada camino k
                for(int i=0;i<capacidad;i++){
                    for(Nodo n=ksp[indiceKsp.get(indKspMasCorto)].getInicio();n.getSiguiente().getSiguiente()!=null;n=n.getSiguiente()){
                        if(G.acceder(n.getDato(),n.getSiguiente().getDato()).getFS()[i].getEstado()==0){
                            OE[i]=0;
                            break;
                        }
                    }
                }
                cont=0; 
                for(int i=0;i<capacidad;i++){
                    if(OE[i]==1){
                        cont++;
                    }else if (OE[i]==0){
                        cont=0;
                    }
                    //si se encontro un bloque valido, tomamos en cuenta el ksp
                    if(cont==demanda.getNroFS()){ //si o si va a encontrar ya que es un ksp en kspUbicados
                        r.setCamino(indiceKsp.get(indKspMasCorto));
                        r.setFin(i);
                        r.setInicio(i - cont + 1);
                        break; //solo el primero que encuentra
                    }
                }
            }
        }

        return r;
    }
    
    public static Resultado Def_FA(GrafoMatriz[][] G, Demanda demanda, ListaEnlazada[] ksp,int capacidad, int a){
        
        //*Definicion de variables las variables
        int cont; // posicion inicial y final dentro del espectro asi como el contador de FSs contiguos disponibles
        int sgteBloque;//bandera para avisar que tiene que ir al siguiente bloque
        
//        int demandaColocada=0; // bandera para controlar si ya se encontro espectro disponible para la demanda.
        int [] OE= new int[capacidad]; //Ocupacion de Espectro.
        ArrayList<ListaEnlazada> kspUbicados = new ArrayList<>();
        ArrayList<Integer> inicios = new ArrayList<>();
        ArrayList<Integer> fines = new ArrayList<>();
        ArrayList<Integer> indiceKsp = new ArrayList<>();

        //Probamos para cada camino, si existe espectro para ubicar la damanda
        int k=0;

        while(k<ksp.length && ksp[k]!=null){
            //Inicializadomos el espectro, inicialmente todos los FSs estan libres
            for(int i=0;i<capacidad;i++){
                OE[i]=1;
            }
            //Calcular la ocupacion del espectro para cada camino k
            for(int i=0;i<capacidad;i++){
                for(Nodo n=ksp[k].getInicio();n.getSiguiente().getSiguiente()!=null;n=n.getSiguiente()){
                   //System.out.println("v1 "+n.getDato()+" v2 "+n.getSiguiente().getDato()+" cant vertices "+G.getCantidadDeVertices()+" i "+i+" FSs "+G.acceder(n.getDato(),n.getSiguiente().getDato()).getFS().length);
                    if(G[ksp[k].getCore()][a].acceder(n.getDato(),n.getSiguiente().getDato()).getFS()[i].getEstado()==0){
                        OE[i]=0;
                        break;
                    }
                }
            }
            
            //encuentra las posibles asignaciones
            cont=0;
            sgteBloque = 0; 
            for(int i=0;i<capacidad;i++){
                if(OE[i]==1 && sgteBloque == 0){
                    cont++;
                }else if (OE[i]==0){
                    cont=0;
                    sgteBloque = 0;
                }
                //si se encontro un bloque valido, tomamos en cuenta el ksp
                if(cont==demanda.getNroFS()){
                    fines.add(i);
                    inicios.add(i - cont + 1);
                    kspUbicados.add(ksp[k]);
                    indiceKsp.add(k);
                    sgteBloque = 1;
                    cont = 0;
//                    break; //solo agrega el primero que encuentra
                }
            }
            k++;
        }
        
        if (kspUbicados.isEmpty()){ //bloqueo
            //System.out.println("Desubicado");
            return null;
        }
        
        //cuenta los cortes para cada posible asiganción
//        int cutsSlot; //cantidad de cortes
        int ind = 0; //aux indice del kspUbicado actual
        int cutAux = 0; //cant de cortes del camino ksp
        int cuts = 999; //el menor corte, 999 como referencia inicial
        Resultado r = new Resultado();
        int DesalineacionFinal = 999;
        
        //vectores con los menores cortes
        ArrayList<Integer> indKSPUbicMenCuts = new ArrayList<>();
        ArrayList<Integer> indKSPUbicMenCutsMenDesalig = new ArrayList<>();
        
        //por cada índice de los posibles caminos de cada KSP ubicado
        for (ListaEnlazada kspUbi : kspUbicados){
            for (Nodo n = kspUbi.getInicio(); n.getSiguiente().getSiguiente() != null; n = n.getSiguiente()) {
                if (inicios.get(ind) != 0 && fines.get(ind) < capacidad - 1) { //para que no tome los bordes sup e inf
                    if (G[kspUbi.getCore()][a].acceder(n.getDato(), n.getSiguiente().getDato()).getFS()[inicios.get(ind) - 1].getEstado() == 1
                            && G[kspUbi.getCore()][a].acceder(n.getDato(), n.getSiguiente().getDato()).getFS()[fines.get(ind) + 1].getEstado() == 1) {
                        cutAux = cutAux + 1;
                    }
                }
            }
            //encuentra el/los menor/es
            if (cutAux < cuts) {
                //si hay un menor al menor, limpia el vector y solo deja ese
                cuts = cutAux;
                indKSPUbicMenCuts.clear();
                indKSPUbicMenCuts.add(ind);
            }else if(cutAux == cuts){
                indKSPUbicMenCuts.add(ind);
            }
            cutAux = 0;
            ind++;
        }
        
        //si hay un solo menor cut entonces es elegido, sino se calcula el alineamiento de los menores
        if (indKSPUbicMenCuts.size() == 1){
            r.setCamino(indiceKsp.get(indKSPUbicMenCuts.get(0)));
            r.setFin(fines.get(indKSPUbicMenCuts.get(0)));
            r.setInicio(inicios.get(indKSPUbicMenCuts.get(0)));
        }else {
            //calcula el desalineamiento de cada uno
            for (int indMenorCuts : indKSPUbicMenCuts) {                
                //calcula el desalineamiento de cada uno
                int Desalineacion = 0;
                int bandEsRuta = 0;
                for (Nodo n = kspUbicados.get(indMenorCuts).getInicio(); n.getSiguiente().getSiguiente() != null; n = n.getSiguiente()) {
                    for (int i = 0; i < G[kspUbicados.get(indMenorCuts).getCore()][a].getCantidadDeVertices(); i++) {
                        if(G[kspUbicados.get(indMenorCuts).getCore()][a].acceder(n.getDato(), i)!=null){ //si son vecinos
                            bandEsRuta = 0;
                            for (Nodo nn = kspUbicados.get(indMenorCuts).getInicio(); nn.getSiguiente().getSiguiente() != null; nn = nn.getSiguiente()) {
                                if(nn.getDato() == i){ //si es parte de la ruta
                                    bandEsRuta = 1;
                                    break;
                                }
                            }
                            if(bandEsRuta == 0){
                                for(int fsd = inicios.get(indMenorCuts); fsd <= inicios.get(indMenorCuts) + demanda.getNroFS() - 1; fsd++){
                                    if (G[kspUbicados.get(indMenorCuts).getCore()][a].acceder(i, n.getDato()).getFS()[fsd].getEstado() == 1) {
                                        Desalineacion = Desalineacion + 1;
                                    } else {
                                        Desalineacion = Desalineacion - 1;
                                    }
                                }
                            }
                        }
                    }
                }

                if (Desalineacion < DesalineacionFinal) {
                    //si hay un menor al menor, limpia el vector y solo deja ese
                    DesalineacionFinal = Desalineacion;
                    indKSPUbicMenCutsMenDesalig.clear();
                    indKSPUbicMenCutsMenDesalig.add(indMenorCuts);
                }else if(Desalineacion == DesalineacionFinal){
                    indKSPUbicMenCutsMenDesalig.add(indMenorCuts);
                }
            }
            
            //si hay un solo menor cut con menor desalineación entonces es elegido, sino envie al shorter KSP y hace first Fit
            if (indKSPUbicMenCutsMenDesalig.size() == 1){
                r.setCamino(indiceKsp.get(indKSPUbicMenCutsMenDesalig.get(0)));
                r.setFin(fines.get(indKSPUbicMenCutsMenDesalig.get(0)));
                r.setInicio(inicios.get(indKSPUbicMenCutsMenDesalig.get(0)));
            }else { //calcula el shorter KSP y hace first Fit  
                //encontrar el ksp más corto
                int tamKspMasCorto = 999;
                int indKspMasCorto = -1;
                for (int indMenCutsMenDesalig : indKSPUbicMenCutsMenDesalig) {
                    if (kspUbicados.get(indMenCutsMenDesalig).getTamanho() - 1 < tamKspMasCorto){
                        indKspMasCorto = indMenCutsMenDesalig;
                        tamKspMasCorto = kspUbicados.get(indMenCutsMenDesalig).getTamanho() - 1;
                    }
                }
                //buscar el indice first fit
                //Inicializadomos el espectro, inicialmente todos los FSs estan libres
                for(int i=0;i<capacidad;i++){
                    OE[i]=1;
                }
                //Calcular la ocupacion del espectro para cada camino k
                for(int i=0;i<capacidad;i++){
                    for(Nodo n=ksp[indiceKsp.get(indKspMasCorto)].getInicio();n.getSiguiente().getSiguiente()!=null;n=n.getSiguiente()){
                        if(G[ksp[indiceKsp.get(indKspMasCorto)].getCore()][a].acceder(n.getDato(),n.getSiguiente().getDato()).getFS()[i].getEstado()==0){
                            OE[i]=0;
                            break;
                        }
                    }
                }
                cont=0; 
                for(int i=0;i<capacidad;i++){
                    if(OE[i]==1){
                        cont++;
                    }else if (OE[i]==0){
                        cont=0;
                    }
                    //si se encontro un bloque valido, tomamos en cuenta el ksp
                    if(cont==demanda.getNroFS()){ //si o si va a encontrar ya que es un ksp en kspUbicados
                        r.setCamino(indiceKsp.get(indKspMasCorto));
                        r.setFin(i);
                        r.setInicio(i - cont + 1);
                        break; //solo el primero que encuentra
                    }
                }
            }
        }

        return r;
    }
    
    /*public static void defragmentarFS(GrafoMatriz G) {                           
        FrecuencySlots[] fs; 
        int count,countInact = 0; 
        for (int i = 0; i < G.getCantidadDeVertices(); i++) { 
            for (int j = 0; j < G.getCantidadDeVertices(); j++) { 
                if (G.acceder(i, j) != null && G.acceder(i, j).getEstado()) {     //ACCEDIMOS AL ENLACE SI ES QUE EXISTE ENLACE (i,j) Y SI ÉSTE SE ENCUENTRA ACTIVO 
                    count = 0;
                    countInact = 0;
                    fs = G.acceder(i, j).getFS(); 
                    System.out.println("ANTES DE DESFRAGMENTAR - Para enlace (" + i + "," + j + ")");
                    /*for (int s = 0; s < fs.length; s++) {
                        System.out.println("Estado: " + fs[s].getEstado() + " con tiempo de vida: "+ fs[s].getTiempo());
                    }*
                    FrecuencySlots[] aux = new FrecuencySlots[fs.length];
                    FrecuencySlots[] inactivos = new FrecuencySlots[fs.length];
                    for (int n = 0; n < fs.length; n++) {
                      // RECORREMOS LA LISTA DE FRECUENCY SLOTS 
                      //  System.out.println("Contador final de cantidad de FS introducidos en enlace es: " + count);
                        if (fs[n].getEstado() == 0) {                              // SI ESTÁ OCUPADO AÑADIMOS A LA LISTA AUXILIAR 
                            aux[count] = fs[n]; 
                            count++; 
                        }else if(fs[n].getEstado() ==1){
                            inactivos[countInact]= fs[n];
                            countInact++;
                        } 
                    }
                        Utilitarios.quicksortTiempo(aux, 0, (count - 1));                        // ORDENAMOS LISTA POR TIEMPO DE VIDA                                
                    //AHORA AGREGAMOS LOS ENLACES INACTIVOS
                    int z=0;
                    while(inactivos[z]!=null){
                        aux[count] = inactivos[z];
                        count++;
                        z++;
                    }
                    // FINALMENTE SUSTITUIMOS LA LISTA FRAGMENTADA (FS) POR LA LISTA DESFRAGMENTADA (AUX) 
                    G.acceder(i, j).setFs(aux);
                    System.out.println("DESPUES DE DESFRAGMENTAR - Para enlace (" + i + "," + j + ")");
                    /*for (int s = 0; s < aux.length; s++) {
                        System.out.println("Estado: " + aux[s].getEstado() + " con tiempo de vida: "+ aux[s].getTiempo());
                    }*
                }
                
              //  System.out.println("Contador final de cantidad de FS introducidos en enlace es: " + count);
            }
            
        }
        System.out.println("FIN DE LA DESFRAGMENTACIÓN DE LOS ENLACES");
    }*/
    
    public static void defragmentarFS(GrafoMatriz G, int fsQuant) {
        int[] indices = new int[fsQuant];                               // Este valor debe ser recibido como parámetro
        int count = 0;
        
        for (int i = 0; i < fsQuant; i++) {    //Recorre los fsQuant FS
            boolean activo = false;
            boolean visitado = false;
            for (int j = 0; j < G.getCantidadDeVertices() && !activo; j++) {
                for (int k = 0; k < G.getCantidadDeVertices() && !activo; k++) {   // Recorremos todos los enlaces
                    if (G.acceder(j, k) != null && G.acceder(j, k).getEstado()) {   // Se verifica si el Enlace existe y está activo
                        if (G.acceder(j, k).getFS()[i].getEstado() == 0) {
                            activo = true;                                         // Si el enlace está activo, el FS no podrá ser incluido para su desfragmentacion
                        }
                        if (G.acceder(j, k).getFS()[i].isVisitado()){
                            visitado = true;
                        }
                    }
                }
            }
            if (!activo && visitado) {
                indices[count] = i;
                count++;
            }
        }
        System.out.println("Se reasignarán " + count + " FS en total");
        if (count > 0) {
            for (int i = 0; i < --count; i++) {                                             //Recorre la lista indices[]
                for (int k = indices[i]; k < fsQuant; k++) {                                             // Recorremos por FS en todos los enlaces
                    for (int n = 0; n < G.getCantidadDeVertices(); n++) {
                        for (int m = 0; m < G.getCantidadDeVertices(); m++) {
                            if (G.acceder(n, m) != null && G.acceder(n, m).getEstado()){    // Se verifica si el Enlace existe y está activo
                                G.acceder(n, m).getFS()[k]= G.acceder(n, m).getFS()[k+1];    // Se sustituye el FS[k] por FS[k+1]
                                if (k == (fsQuant-2)) {                                              // En FS[fsQuant] seteamos libre al 
                                    G.acceder(n, m).getFS()[fsQuant-1].setEstado(1);
                                    G.acceder(n, m).getFS()[fsQuant-1].setTiempo(0);
                                    G.acceder(n, m).getFS()[fsQuant-1].setConexion(1);
                                    G.acceder(n, m).setUtilizacionFS(fsQuant-1,0);                 // Hay que setear la utilización
                                }
                            }
                        }
                    }
                }
            }
        }
        System.out.println("FIN DE LA DESFRAGMENTACIÓN DE FS");
    }  
    
    
    public static void defragmentarFS2(GrafoMatriz G, int fsQuant, ArrayList<Resultado> resultados, ArrayList<Integer> tiemposDeVida, ArrayList<ListaEnlazada> rutas) {
        
        System.out.println("INICIO DE DESFRAGMENTACIÓN DE FS");
        
        // Iteramos por los slots de frecuencia, en orden inverso.
        for(int fs = fsQuant-1; fs>=0; fs--) {

            for(int indiceDemanda = 0; indiceDemanda<resultados.size(); indiceDemanda++) {
                // Se busca demandas en los FS.
                Resultado resultado = resultados.get(indiceDemanda);
                if(resultado.getFin() == fs) {
                    
                    
                    System.out.println("La demanda " + indiceDemanda + " esté entre los fs " + resultados.get(indiceDemanda).getInicio()+ " y " + resultados.get(indiceDemanda).getFin());
                    for(Nodo n=rutas.get(indiceDemanda).getInicio();n.getSiguiente().getSiguiente()!=null;n=n.getSiguiente()) {
                        
                        

                        // Actualizar FS en enlaces
                        Enlace asd = G.acceder(n.getDato(), n.getSiguiente().getDato());                        
                        
                    }
                    // Actualizar FS en resultados.
                }
            }
        }
        
        System.out.println("FIN DE LA DESFRAGMENTACIÓN DE FS");
    }
    
    public static void defragmentarFS(GrafoMatriz G, int fsQuant, ArrayList<Resultado> resultados, ArrayList<Integer> tiemposDeVida, ArrayList<ListaEnlazada> rutas) {
        
        System.out.println("INICIO DE DESFRAGMENTACIÓN DE FS");
        
        // Matriz auxiliar para almacenar el mayor sin utilizar
        Integer[][] matrizFS = new Integer[G.getVertices().length][G.getVertices().length];
        for(Integer i = 0; i<G.getVertices().length; i++) {
            for(Integer j = 0; j<G.getVertices().length; j++) {
                matrizFS[i][j] = fsQuant-1;
            }
        }
        
        // Iteramos por los slots de frecuencia, en orden inverso.
        for (int fs = fsQuant - 1; fs >= 0; fs--) {
            for (int indiceDemanda = 0; indiceDemanda < resultados.size(); indiceDemanda++) {
                ListaEnlazada ruta = rutas.get(indiceDemanda);
                Resultado resultado = resultados.get(indiceDemanda);
                if (resultado.getFin() == fs) {
                    // Por cada enlace que utiliza la demanda
                    Boolean puedeMover = Boolean.TRUE;
                    Nodo n = ruta.getInicio();
                    int fsIni = -1, fsFin = -1;
                    while (n.getSiguiente() != null && n.getSiguiente().getDato() != 99 && puedeMover) {
                        Enlace enlace = G.acceder(n.getDato(), n.getSiguiente().getDato());
                        fsFin = matrizFS[n.getDato()][n.getSiguiente().getDato()];
                        int fsWidth = resultado.getFin() - resultado.getInicio();
                        fsIni = fsFin - fsWidth;
                        if (!hayRangoLibre(enlace.getFS(), fsIni, fsFin)) {
                            // Reinicia para el siguiente FS
                            puedeMover = false;
                            n = ruta.getInicio();
                        }
                        n = n.getSiguiente();
                    }
                    
                    if(puedeMover && fsIni > -1 && fsFin > -1) {
                        for (Nodo nod = rutas.get(indiceDemanda).getInicio(); nod.getSiguiente().getSiguiente() != null; nod = nod.getSiguiente()) {
                            // Asignar nuevos frecuency slots
                            for(int fsActual = fsIni; fsActual<= fsFin; fsActual++) {
                                G.acceder(nod.getDato(), nod.getSiguiente().getDato()).getFS()[fsActual].setEstado(0);
                                matrizFS[nod.getDato()][nod.getSiguiente().getDato()] = fsIni-1;
                                    /*System.out.println();
                                for(int asd = 0; asd<matrizFS.length; asd++) {
                                    for(int qwe = 0; qwe<matrizFS[asd].length; qwe++) {
                                        System.out.print(matrizFS[asd][qwe] + ", ");
                                    }
                                    System.out.println();
                                }*/
                            }
                            
                            // Eliminar frecuency slots antiguos
                            int fsIniOriginal = resultados.get(indiceDemanda).getInicio();
                            int fsFinOriginal = resultados.get(indiceDemanda).getFin();
                            for(int fsActual = fsIniOriginal; fsActual<= fsFinOriginal; fsActual++) {
                                G.acceder(nod.getDato(), nod.getSiguiente().getDato()).getFS()[fsActual].setEstado(1);
                            }
                            
                            resultados.get(indiceDemanda).setInicio(fsIni);
                            resultados.get(indiceDemanda).setInicio(fsFin);
                            
                        }
                        System.out.println("Demanda "+ indiceDemanda +" movida al FS " + fsIni + "-" + fsFin);
                    }
                }
            }
        }
        System.out.println("FIN DE LA DESFRAGMENTACIÓN DE FS");
    }
    
    private static Boolean hayRangoLibre(FrecuencySlots[] slots, int fsScanStart, int fsScanEnd) {
        for(int fs=fsScanStart; fs<=fsScanEnd; fs++) {
            if(Utilitarios.estaOcupado(slots[fs])) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }
    
    public class Rango {
        private int desde;
        private int hasta;

        public int getDesde() {
            return desde;
        }

        public void setDesde(int desde) {
            this.desde = desde;
        }

        public int getHasta() {
            return hasta;
        }

        public void setHasta(int hasta) {
            this.hasta = hasta;
        }
    }
}

