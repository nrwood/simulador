package EON;

/**
 * <p>
 * Clase creada para que EONS soporte Modulacion de trafico El unico atributo de
 * la clase es la matriz tablaModulacion que es una matriz de 4x3 que almacena
 * cual es el alcance, y la eficiencia en bit/s/Hz. En este caso, se considera
 * la eficienca de transporte de datos segun la modulacion por ranura de FS. El
 * utilizado en esta simulacion sigue las pruebas realizdas en este paper:
 * "Traffic and Power-Aware Protection Scheme in Elastic Optical Networks".
 * </p>
 *
 * <p>
 * <table border="1">
 * <th>
 * <td>Tipo de Modulacion</td>           <td>Alcance (km)</td>             <td>Eficiencia (Gbps)</td>
 * </th>
 * <tr><td>1</td> <td>1 (BPSK)</td>                    <td>4000</td>                        <td>12.5</td> </tr>
 * <tr><td>2</td><td>2 (QPSK)</td>                     <td>2000</td>                        <td>25</td></tr>
 * <tr><td>3</td><td>3 (8-QAM)</td>                    <td>1000</td>                        <td>37.5</td></tr>
 * <tr><td>4</td><td>4 (16- QAM)</td>                  <td>500</td>                         <td>50</td></tr>
 * </table>
 * </p>
 *
 * @author Fer
 *
 */
public class Modulacion {

    private final double[][] tablaModulacion;

    public Modulacion() {
        double[][] tabla = {{1, 4000, 12.5}, {2, 2000, 25}, {3, 1000, 37.5}, {4, 500, 50}}; //tabla de modulacion predeterminada
        this.tablaModulacion = tabla;
    }

    public Modulacion(double[][] t) { //definido por el usuario
        this.tablaModulacion = t;
    }

    /*
    * Obtiene el tipo de modulacion segun la distancia(km) siguiendo la tablaModulacion
     */
    public int getTipoDeModulacion(int distancia) {

        if (distancia <= tablaModulacion[3][1]) { // se puede 16-QAM?
            return (int) tablaModulacion[3][0];
        } else if (distancia <= tablaModulacion[2][1]) { // se puede 8-QAM?
            return (int) tablaModulacion[2][0];
        } else if (distancia <= tablaModulacion[1][1]) { // se puede QPSK?
            return (int) tablaModulacion[1][0];
        }
        return 1; // si ninguno se pudo, se considera BPSK
    }

    /*
    * Obteniene el Nro de FS necesarios segun la distancia entre el nodo origen y fin, y el ancho de banda requerido.
    * La formula utilizada fue obtenida de "Distance-adaptive routing and spectrum assignment in  OFDM-based flexible transparent optical networks". 
    * Importante decir que en este paper y en otros se realiza en siguiente pasa cada par de nodos de camino, mientras que en otros caso, solo se realiza
    * para el camino completo, aque se considera para el camino completo
     */
    public int getNroFS(int B, int distancia) {
        int modulacion = getTipoDeModulacion(distancia);
        Double N = B / (modulacion * 12.5);
        if (N < 1) {
            N++;
        }
        return N.intValue();
    }

    /*
    * Para los casos en que no se considera modulacion, por defecto se utiliza  BPSK.
     */
    public int getNroFSbpsk(int B) {
        Double N;
        N = B / (12.5);
        if (N < 1) {
            N++;
        }
        return N.intValue();
    }

}
