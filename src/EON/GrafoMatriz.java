package EON;

import EON.Utilitarios.Utilitarios;
import java.util.ArrayList;

/**
 *
 * @author Team Delvalle El grafo en su forma de matriz de adyacencia para
 * representar la topologia de una red. Almacena: Los datos de los enlaces para
 * cada vertice i,j La cantidad de vertices La capacidad total que es la catidad
 * de FSs disponibles por enlace en la red. El ancho de cada FS.
 */
public class GrafoMatriz {

    private final Enlace[][] vertices;
    private final boolean[] marcas;
    private final int cantidadVertices;
    private int capacidadTotal;
    private double anchoFS;

    public GrafoMatriz(int V) {

        this.marcas = new boolean[V];
        this.vertices = new Enlace[V][V];
        this.cantidadVertices = V;
        this.capacidadTotal = 0;
        this.anchoFS = 0;

    }

    public GrafoMatriz() {

        this.cantidadVertices = 0;
        this.marcas = null;
        this.vertices = null;
    }

    public GrafoMatriz insertarDatos(double[][][] v) {
        double nro, distancia, espectro, ancho;

        for (int i = 0; i < this.vertices.length; i++) {
            for (int j = 0; j < this.vertices.length; j++) {
                if (v[i][j][1] != 0) {
                    nro = v[i][j][0];
                    distancia = v[i][j][1];
                    espectro = v[i][j][2];
                    this.capacidadTotal = (int) espectro;
                    ancho = v[i][j][3];
                    this.anchoFS = ancho;
                    this.vertices[i][(int) nro] = new Enlace(i, (int) nro, (int) espectro, (int) distancia, ancho);
                    this.vertices[(int) nro][i] = new Enlace((int) nro, i, (int) espectro, (int) distancia, ancho);
                }
            }
        }
        return this;
    }

    public void marcar(int i) {
        marcas[i] = true;
    }

    public boolean getMarca(int i) {
        return marcas[i];
    }

    public Enlace acceder(int i, int j) {
        return this.vertices[i][j];
    }

    public int getCantidadDeVertices() {
        return this.cantidadVertices;
    }

    public int getCapacidadTotal() {
        return this.capacidadTotal;
    }

    public double getAnchoFS() {
        return this.anchoFS;
    }

    public void setCapacidadTotal(int c) {
        this.capacidadTotal = c;
        for (int i = 0; i < this.vertices.length; i++) {
            for (int j = 0; j < this.vertices.length; j++) {
                if (this.acceder(i, j) != null) {
                    this.acceder(i, j).setEspectro(c);
                }
            }
        }
    }

    public void setAnchoFS(double a) {
        this.anchoFS = a;
        for (int i = 0; i < this.vertices.length; i++) {
            for (int j = 0; j < this.vertices.length; j++) {
                if (this.acceder(i, j) != null) {
                    this.acceder(i, j).setAnchoFS(a);
                }
            }
        }
    }

    public int getCantidadEnlaces() {
        int cont = 0;
        ArrayList<Integer> lista = new ArrayList<>();
        for (int i = 0; i < this.cantidadVertices; i++) {
            for (int j = 0; j < this.cantidadVertices; j++) {
                if (this.acceder(i, j) != null) {
                    if (!Utilitarios.isInList(lista, j)) {
                        cont++;
                    }
                }
            }
            lista.add(i);
        }
        return cont;
    }

    public void restablecerFS() {
        for (int i = 0; i < this.getCantidadDeVertices(); i++) {
            for (int j = 0; j < this.getCantidadDeVertices(); j++) {
                if (this.acceder(i, j) != null) {
                    for (FrecuencySlots f : this.acceder(i, j).getFS()) {
                        f.setEstado(1);
                        f.setTiempo(0);
                        f.setPropietario(-1);
                    }
                }
            }
        }
    }

    public double entropia() {

        double uelink = 0;
        double entropy;
        int countlinks = 0;
        for (int i = 0; i < this.getCantidadDeVertices(); i++) {
            for (int j = 0; j < this.getCantidadDeVertices(); j++) {
                int UEcont = 0;
                if (this.acceder(i, j) != null) {
                    for (int kk = 0; kk < this.acceder(i, j).getFS().length - 1; kk++) {
                        //System.out.println("fs tamano: "+ this.acceder(i, j).getFS().length);
                        //System.out.println("fs tamano: "+ this.acceder(i, j).getFS()[kk]);
                        //System.out.println("fs-utili--> "+ this.acceder(i, j).getFS()[kk].getEstado() +"::"+ this.acceder(i, j).getUtilizacion()[kk]);
                        if (this.acceder(i, j).getFS()[kk].getEstado() != this.acceder(i, j).getFS()[kk + 1].getEstado()) {
                            UEcont++;
                        }
                    }
                    uelink = uelink + (double) UEcont;//(this.acceder(i, j).getFS().length-1));
                    countlinks++;
                    //System.in.read();
                }
            }
        }
        //System.out.println("cantidad de links::::: "+ countlinks);
        entropy = uelink / countlinks;
        return entropy;
    }

    public double entropiaShanon(int capacidad) {

        double shannonEntropyLink = 0.0;
        double shanonEntropy;
        double NFbloquesLibres = 0;
        double contLibres = 0;
        double LScantFS = capacidad;
        int countlinks = 0;
        for (int i = 0; i < this.getCantidadDeVertices(); i++) {
            for (int j = 0; j < this.getCantidadDeVertices(); j++) {
                if (this.acceder(i, j) != null) {
                    for (int kk = 0; kk < this.acceder(i, j).getFS().length - 1; kk++) {
                        //System.out.println(this.acceder(i,j).getFS()[kk].getEstado());
                        if (this.acceder(i, j).getFS()[kk].getEstado() == 1) {
                            contLibres++;
                        }
                        if (this.acceder(i, j).getFS()[kk].getEstado() != this.acceder(i, j).getFS()[kk + 1].getEstado()) {
                            if (this.acceder(i, j).getFS()[kk].getEstado() == 1) {
                                //System.out.println("libres"+contLibres);
                                shannonEntropyLink += ((contLibres / LScantFS) * Math.log((LScantFS / contLibres)));

                                //System.out.println("selink"+shannonEntropyLink);
                                contLibres = 0;
                            }
                        }
                    }
                    if (this.acceder(i, j).getFS()[this.acceder(i, j).getFS().length - 1].getEstado() == 1) {
                        contLibres++;
                        //System.out.println("LIBRESFIN"+contLibres);
                        shannonEntropyLink += ((contLibres / LScantFS) * Math.log((LScantFS / contLibres)));
                        //System.out.println("selinkFIN"+shannonEntropyLink);
                        contLibres = 0;
                    }
                    countlinks++;
                    //System.in.read();
                }
            }
        }
        //System.out.println("cantidad de links::::: "+ countlinks);

        //System.out.println("SELINK::::: "+ shannonEntropyLink);
        shanonEntropy = shannonEntropyLink / countlinks;

        //System.out.println("RESUl::::: "+ shanonEntropy);
        //System.out.println(1/0);
        return shanonEntropy;
    }

    public double ABP(int capacidad) {

        double abpLink = 0.0;
        double arriba = 0.0;
        double abpRed;
        double abajo;
        double contLibres = 0;
        double FSLibres = 0;
        int countlinks = 0;
        for (int i = 0; i < this.getCantidadDeVertices(); i++) {
            for (int j = 0; j < this.getCantidadDeVertices(); j++) {
                if (this.acceder(i, j) != null) {
                    //para calcular arriba
                    //primera sumatoria (F)
                    for (int kk = 0; kk < this.acceder(i, j).getFS().length - 1; kk++) {
                        //System.out.println(this.acceder(i,j).getFS()[kk].getEstado());
                        if (this.acceder(i, j).getFS()[kk].getEstado() == 1) {
                            contLibres++;
                            FSLibres++;
                        }
                        if (this.acceder(i, j).getFS()[kk].getEstado() != this.acceder(i, j).getFS()[kk + 1].getEstado()) {
                            if (this.acceder(i, j).getFS()[kk].getEstado() == 1) {
                                //System.out.println("libres"+contLibres);
                                //segunda sumatoria (G)
                                for (int G = 1; G <= 8; G++) {
                                    arriba += (int) (contLibres / G);
                                }

                                //System.out.println("selink"+shannonEntropyLink);
                                contLibres = 0;
                            }
                        }
                    }
                    if (this.acceder(i, j).getFS()[this.acceder(i, j).getFS().length - 1].getEstado() == 1) {
                        contLibres++;
                        FSLibres++;
                        //System.out.println("LIBRESFIN"+contLibres);
                        for (int G = 1; G <= 8; G++) {
                            arriba += (int) (contLibres / G);
                        }
                        //System.out.println("selinkFIN"+shannonEntropyLink);
                        contLibres = 0;
                    }
                    abajo = 0;
                    for (int G = 1; G <= 8; G++) {
                        abajo += (int) (FSLibres / G);
                    }
                    abpLink += (1 - (arriba / abajo));
                    FSLibres = 0;
                    arriba = 0;
                    countlinks++;
                }
            }
        }
        //System.out.println("cantidad de links::::: "+ countlinks);

        //System.out.println("SELINK::::: "+ shannonEntropyLink);
        abpRed = abpLink / countlinks;

        //System.out.println("RESUl::::: "+ shanonEntropy);
        //System.out.println(1/0);
        return abpRed;
    }

    public void ResetRed() {
        for (int ii = 0; ii < this.getCantidadDeVertices(); ii++) {
            for (int jj = 0; jj < this.getCantidadDeVertices(); jj++) {
                if (this.acceder(ii, jj) != null) {
                    for (FrecuencySlots f : this.acceder(ii, jj).getFS()) {
                        f.setEstado(1);
                        f.setTiempo(0);
                        //System.out.println(G[8].acceder(ii, jj).getVertice1()+"......");
                    }
                }
            }
        }
    }

    public Enlace[][] getVertices() {
        return vertices;
    }
}
